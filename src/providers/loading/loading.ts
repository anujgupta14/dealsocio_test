import { Injectable, ViewChild } from "@angular/core";
import {
	App,
	LoadingController,
	NavController,
	ToastController
} from "ionic-angular";

/*
  Generated class for the LoadingProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class LoadingProvider {
	@ViewChild("myNav") nav: NavController;
	isLoading: boolean = false;
	loading: any = "";
	constructor(
		private toastCtrl: ToastController,
		private app: App,
		private loadingCtrl: LoadingController
	) {}

	showLoading(message: string = "") {
		this.loading = this.loadingCtrl.create({
			content: message,
			enableBackdropDismiss: true
		});
		this.loading.present();
		this.isLoading = true;
		return this.loading;
	}

	async hideLoading() {
		if (this.isLoading) await this.loading.dismiss();
		this.isLoading = false;
	}
	showNotification(mymessage, type, dismiss) {
		let nav = this.app.getActiveNavs()[0];
		let toast = this.toastCtrl.create({
			message: mymessage,
			duration: 2000,
			cssClass: type,
			position: "bottom"
		});
		if (dismiss) {
			toast.onDidDismiss(() => {
				console.log("Dismissed toast");
				nav.push(dismiss);
			});
		}
		toast.present();
	}
	popNotification(mymessage, type) {
		let nav = this.app.getActiveNavs()[0];
		let toast = this.toastCtrl.create({
			message: mymessage,
			duration: 2000,
			cssClass: type,
			position: "bottom"
		});
		toast.onDidDismiss(() => {
			console.log("Dismissed toast");
			nav.pop();
		});
		toast.present();
	}
}
