import {Http, Headers, RequestOptions} from '@angular/http';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { DomSanitizer } from '@angular/platform-browser';
import 'rxjs/add/operator/map';
import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer';
import {LoadingController, ModalController, Platform} from "ionic-angular";
import {Observable} from "rxjs/Observable";
import {LoadingProvider} from "../loading/loading";
import {AppVersion} from '@ionic-native/app-version';

@Injectable()
export class GlobalProvider {
  serverURL = 'http://api.dealsocio.com';
  baseURL = 'https://s3.ap-south-1.amazonaws.com/';
  experlist: any;
  userDetails: any = {};
    signupInfo:any = {};
    loginInfo:any={};
    searchUser:any="";
    option:any={};
  testUserInfo:any={};
    configData:any;
  makebackActive:boolean=false;
  experiance: any;
  public option2 :any={};
  location: any;
  myAddress:any=[];
  data: any;
  //unreadNotifications:number = 0;
  notificationRedirect: any = {page:"", data:{}};

  /*public headers : any;
    public headersMultipart : any;
    public options : any;
    public optionsMultipart : any;*/

  public headers = new Headers({
    'Content-Type': 'application/json;charset=UTF-8',
    'token': '',  //08a9752c-66a8-4744-a128-3e66c970d316
    'userid': ''  //5ab0a62b8eb59c765b0e2d04
  });
  public headersMultipart = new Headers({
    //'Content-Type': 'multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW',
    'token': '',  //2915cd58-cb71-4a4f-8aab-f1329340617a
    'userid': ''   //5a3e69af5fd6bf0c51e15dcf
  });
  public options = new RequestOptions({
    headers: this.headers
  });
  public optionsMultipart = new RequestOptions({
    headers: this.headersMultipart
  });

  constructor(public modalCtrl: ModalController,public platform: Platform,private appVersion: AppVersion,public loadingService:LoadingProvider, public http: Http,public loading: LoadingController,private transfer: FileTransfer, public storage: Storage, private sanitization: DomSanitizer) {
    console.log('Hello GlobalProvider Provider');

    this.storage.get('userDetails').then((data) => {
      if (data) {
        this.userDetails = data;
        console.log("saved user details");
        console.log(this.userDetails);
        /*	this.headers.append("token",data.session.token);
                this.headers.append("userid",data.session.userId); */

        /*this.headers = new Headers({
                'Content-Type': 'application/json;charset=UTF-8',
                'token': data.session.token,
                'userid': data.session.userId
            });
            this.headersMultipart = new Headers({
                'token': data.session.token,
                'userid': data.session.userId
            });

              this.options = new RequestOptions({
                headers: this.headers
            });
            this.optionsMultipart = new RequestOptions({
                headers: this.headersMultipart
              });*/

        this.headers.delete("token");
        this.headers.delete("userid");
        this.headers.append("token", this.userDetails.session.token);
        this.headers.append("userid", this.userDetails.session.userId);
        console.log(this.headers);

        this.headersMultipart.delete("token");
        this.headersMultipart.delete("userid");
        this.headersMultipart.append("token", this.userDetails.session.token);
        this.headersMultipart.append("userid", this.userDetails.session.userId);

        this.initExper();
      }
    });
  }
     fileTransfer: FileTransferObject = this.transfer.create();
  sanatizeurl(image_url) {
    return this.sanitization.bypassSecurityTrustStyle(`url(${image_url})`);
  }


  // userloginfacebook(accesstoken) {
  //   console.log(accesstoken);
  //   let param_json = {
  //     "accessToken": accesstoken,
  //     /*"accessToken": "EAAGwoSZBtMu4BAJwlx1caKFVZAFE9KMVIXV1dHoh8jtI1evkwFtOkem1kglS99OGT2FwIssLCxoEM3uoTyTyT6c1OqwKdCJFKG0vltiStVIdEZCQhwQrpdflAOEMp6tJpJm1bPK02qIUC0lZCZBGoZB0T7xkyOhdMuszHaYAiS14wi5q3kmEPZAthmJNfgX9DP4IFgv5qT9ECEyQl1N9evc",*/
  //     "email": "",
  //     "socialAppType": 10,
  //   }
  //   return this.http
  //     .post(this.serverURL + '/auth/login', param_json, this.options)
  //     .map(res => res.json());
  // }

  userloginGoogle(accesstoken) {
    console.log(accesstoken);
    let param_json = {
      "accessToken": accesstoken,
      "email": "",
      "socialAppType": 20,
    }
    return this.http
      .post(this.serverURL + '/auth/login', param_json, this.options)
      .map(res => res.json());
  }

  getexper() {
    let param_json = {};
    return this.http
      .post(this.serverURL + '/exp/get/all', param_json, this.options)
      .map(out => out.json());
  }
    newexper(data){
        let param_json = {
            "placeId" : data
        };
        return this.http
            .post(this.serverURL + '/exp/get/all', param_json, this.options)
            .map(out => out.json());
    }

  initExper() {
    this.getexper()
      .subscribe(data => {
        this.experlist = data.elements;
        //console.log(data);
        this.experiance = this.experlist[0];
        console.log("Information of experiances", this.experlist[0])
      });
  }

  getmyprof() {
    console.log(this.options);
    let param_json = {};
    return this.http
      .post(this.serverURL + '/user/get/myProfile', param_json, this.options)
      .map(out => out.json());
  }
    updateNotification(toggleValue) {
        console.log(this.options);
        let param_json = {
            'notificationFlag' : toggleValue
        };
        return this.http
            .post(this.serverURL + '/user/changeNotificationFlag', param_json, this.options)
            .map(out => out.json());
    }

  getphoneotp(phone) {
    let param_json = {
      "phoneNo": phone,
      "isdCode": "91"
    };
    return this.http
      .post(this.serverURL + "/user/phone/sendOtp", param_json, this.options)
      .map(out => out.json());
  }

  verifyotp(otp) {
    let param_json = {
      "otp": otp
    };
    return this.http
      .post(this.serverURL + "/user/phone/verifyOtp", param_json, this.options)
      .map(out => out.json());
  }
sendemailotp(myEmail){
    let param_json = {"email":myEmail};
    return this.http
        .post(this.serverURL + "/user/email/sendOtp", param_json, this.options)
        .map(out => out.json());
}
    verifyemailotp(otp){
        let param_json = {
            "otp": otp
        };
        return this.http
            .post(this.serverURL + "/user/email/verifyOtp", param_json, this.options)
            .map(out => out.json());
    }
  updateinfo() {
    let param_json = {
      "id": "5a3e69af5fd6bf0c51e15dcf",
      "aboutYou": "edition",
      "firstName": "",
      "lastName": "roy",
      "dob": 666901211000,
      "maxDistance": 40
    };
    return this.http
      .post(this.serverURL + "/user/update/myInfo", param_json, this.options)
      .subscribe(data => {
        console.log(data['_body']);
      }, error => {
        console.log(error);// Error getting the data
      });
  }

  getflyerfeed(lat, long, storeId = '',myPageNo) {
    console.log(storeId);
    console.log(lat,long);
    console.log(myPageNo);
    console.log(this.options);
    this.option2=this.options;
      this.storage.set(this.option2, this.options).then(() => {
      });
    let param_json = {
      "location": {
        "coordinates": [
            long,lat
        ]
      },
      "storeId": storeId,
      "pageInfo": {
        "pageNumber": myPageNo,
        "pageSize": 5
      },
      "maxDistance": 20
    };
    return this.http
      .post(this.serverURL + "/flyer/getFlyerFeed", param_json, this.options)
      .map(out => out.json());
  }

  createFlyer(storeId, offerId, publicFlag,  lattitude,longitude, expiryDate, experienceId, des,planInfo) {
    console.log(longitude);
    console.log(lattitude);
    let param_json = {
      "storeId": storeId,
      "offerId": offerId,
      "publicFlag": publicFlag,
      "longitude": longitude,
      "lattitude": lattitude,
      "expiryDate": expiryDate,
      "experienceId": [experienceId],
      "description": des,
        "name":planInfo
    };
    return this.http
      .post(this.serverURL + "/flyer/create", param_json, this.options)
      .map(out => out.json());
  }

  storefeed(exp, lat, long, pageNo) {

    let param_json = {
      "experienceId": this.experiance.id,
      "location": {
        "coordinates": [
          long,
          lat
        ]
      },
      "pageInfo": {
        "pageNumber": pageNo,
        "pageSize": 10
      },
      "maxDistance": 2000000
    };
    return this.http
      .post(this.serverURL + "/store/get/storeFeed", param_json, this.options)
      .map(out => out.json());
  }

  getuserbyid(id) {
    let param_json = {
      "id": id
    };
    return this.http
      .post(this.serverURL + "/user/get/userById", param_json, this.options)
      .map(out => out.json());
  }

  getstorebyid(id) {
    let param_json = {
      "id": id,
        "location": {
            "coordinates": this.myAddress
        }
    };
    return this.http
      .post(this.serverURL + "/store/get/storeById", param_json, this.options)
      .map(out => out.json());
  }

  getinterstbyid(id) {
    let param_json = {
      "flyerId": id
    };
    return this.http
      .post(this.serverURL + "/flyer/interest", param_json, this.options)
      .map(out => out.json());
  }

  avoidbyid(id) {
    let param_json = {
      "flyerId": id
    };
    return this.http
      .post(this.serverURL + "/flyer/uninterest", param_json, this.options)
      .map(out => out.json());
  }

  getmyflyers(mypage) {
    let param_json = {
      "pageInfo": {
        "pageNumber": mypage,
        "pageSize": 10
      }
    };
    return this.http
      .post(this.serverURL + "/flyer/getMyFlyerFeed", param_json, this.options)
      .map(out => out.json());
  }

  getlistrequestbyid(flyerId) {
    let param_json = {
      "flyerId": flyerId
    };
    return this.http
      .post(this.serverURL + "/flyer/getRequestedUsers", param_json, this.options)
      .map(out => out.json());
  }

  getlistacceptbyid(flyerId) {
    let param_json = {
      "flyerId": flyerId
    };
    return this.http
      .post(this.serverURL + "/flyer/getAcceptedUsers", param_json, this.options)
      .map(out => out.json());
  }

  removeParticipant(flyerId, userId) {
    let param_json = {
      "flyerId": flyerId,
      "userId": userId
    };
    return this.http
      .post(this.serverURL + "/flyer/removeParticipant", param_json, this.options)
      .map(out => out.json());
  }

  acceptrejectflyer(FlyerId, User, status) {
    let param_json = {
      "flyerId": FlyerId,
      "requestedUserId": User,
      "acceptReject": status

    };
    return this.http
      .post(this.serverURL + "/flyer/acceptRejectRequest", param_json, this.options)
      .map(out => out.json());
  }

  getlistaccepted() {
    let param_json = {};
    return this.http
      .post(this.serverURL + "/flyer/getAcceptedFlyers", param_json, this.options)
      .map(out => out.json());
  }

  disableflyer(id) {
    let param_json = {
      "flyerId": id
    };
    return this.http
      .post(this.serverURL + "/flyer/disable", param_json, this.options)
      .map(out => out.json());
  }

  updateflyer() {
    let param_json = {
      "flyerId": "5a7b06fc9a6a6f66144bf169",
      "expiryDate": "1583628400000",
      "description": "brrraaah"
    };
    return this.http
      .post(this.serverURL + "/flyer/updateFlyer", param_json, this.options)
      .subscribe(data => {
        console.log(data['_body']);
      }, error => {
        console.log(error);// Error getting the data
      });
  }

  removeSelfFromAcceptedFlyer(flyerid) {
    let param_json = {
      "flyerId": flyerid
    };
    return this.http
      .post(this.serverURL + "/flyer/removeSelfFromAcceptedFlyer", param_json, this.options)
      .map(out => out.json());
  }

  addProfile() {
    let param_json = {
      "file": "file"
    };
    return this.http
      .post(this.serverURL + "/user/image/addProfile", param_json, this.options)
      .subscribe(data => {
        console.log(data['_body']);
      }, error => {
        console.log(error);// Error getting the data
      });
  }

  addPicture(file) {
    let param_json = {
      "file": file
    };

    return this.http
      .post(this.serverURL + "/user/image/add", param_json, this.optionsMultipart)
      .map(out => out.json());
  }
    uploadIMage(fileurl,myoption){
    console.log("inside image upload function");
    console.log(fileurl);
    console.log(myoption);
        return this.fileTransfer.upload(fileurl, this.serverURL + "/user/image/add", myoption,true)
            .then((data) => {
              console.log(data);
                return data;
                // success
            }, (err) => {
              console.log(err);
                return err;
            })
    }

     uploadchatImage(image,option){
            this.loadingService.showLoading("Image uploading");
            console.log(this.serverURL + "/chat/addFile");
            console.log(option);
            console.log(image)
            return this.fileTransfer.upload(image, this.serverURL + "/chat/addFile", option)
                .then((data) => {
                    console.log(data);
                    this.loadingService.hideLoading();
                    return data;
                    // success
                }, (err) => {
                    console.log(err);
                      this.loadingService.hideLoading();
                    return err;
                })
        }

  removePicture(myPhotoId) {
    let param_json = {
      "photoId": myPhotoId,
      "isProfilePic": false
    };
    return this.http
      .post(this.serverURL + "/user/image/remove", param_json, this.options)
        .map(out => out.json());
  }


   makeprofilepicture(photoId){
   let param_json = {
         "photoId": photoId
       };
        return Observable.create(observer => {
            this.http.post(this.serverURL + "/user/image/makeProfile", param_json, this.options)
                .map(res => res.json())
                .subscribe(data => {
                    observer.next(data);
                    observer.complete();
                }, error => {
                    console.log(error);
                    observer.next(false);
                    observer.complete();
                }, () => {
                    console.log('view was not dismissed');
                });
        });
    }

  contactRequestListById() {
    let param_json = {};
    return this.http
      .post(this.serverURL + "/user/get/contactRequestListById", param_json, this.options)
      .map(out => out.json());
  }

  blockList() {
    let param_json = {};
    return this.http
      .post(this.serverURL + "/user/get/blockList", param_json, this.options)
      .map(out => out.json());
  }

  numCodeList() {
    let param_json = {};
    return this.http
      .post(this.serverURL + "/country/getAll", param_json, this.options)
      .map(out => out.json());
  }

  blockUserById(id) {
    let param_json = {
      "id": id
    };
    return this.http
      .post(this.serverURL + "/user/blockUserById", param_json, this.options)
      .map(out => out.json());
  }

  unblockUserById(id) {
    let param_json = {
      "id": id
    };
    return this.http
      .post(this.serverURL + "/user/unblockUserById", param_json, this.options)
      .map(out => out.json());
  }

  contactListById(id) {
    let param_json = {
      "id": id
    };
    return this.http
      .post(this.serverURL + "/user/get/contactListById", param_json, this.options)
      .map(out => out.json());
  }
    othersFrdListById(id) {
        let param_json =  {
            "friendUserId" : id,
            "excludeMe" : false
        };
        return this.http
            .post(this.serverURL + "/user/get/connectionsOfFriend", param_json, this.options)
            .map(out => out.json());
    }
  myLocation(long, lat) {
      this.myAddress=[long, lat];
    let param_json = {
      "location": {
        "coordinates": [long, lat]
      }
    };
    return this.http
      .post(this.serverURL + "/user/update/myLocation", param_json, this.options)
      .map(out => out.json());
  }

  connectionRequestById(id, action) {
    let param_json = {
      "id": id,
      "action": action
    };
    return this.http
      .post(this.serverURL + "/user/send/connectionRequestById", param_json, this.options)
      .map(out => out.json());
  }

  actiononrequest(id, accepted) {
    let param_json = {
      "id": id,
      "accepted": accepted
    };
    return this.http
      .post(this.serverURL + "/user/get/contactRequestActionById", param_json, this.options)
      .map(out => out.json());
  }

  removeContactById(id) {
    let param_json = {
      "id": id
    };
    return this.http
      .post(this.serverURL + "/user/get/removeContactById", param_json, this.options)
      .map(out => out.json());
  }

  reportUserById(id, message,myFlyerId) {
    let param_json = {
      "id": id,
      "message": message,
        "flyerId":myFlyerId
    };
    console.log(param_json);
    return this.http
      .post(this.serverURL + "/user/get/reportUserById", param_json, this.options)
      .map(out => out.json());
  }

  reportUserByIdFlyer(id, message, flyer) {
    let param_json = {
      "id": id,
      "message": message,
      "flyerId": flyer
    };
    return this.http
      .post(this.serverURL + "/user/get/reportUserById", param_json, this.options)
      .map(out => out.json());
  }

  searchProduct(val, flag) {
    let param_json = {
      "query": val,
      "city": null,
      "tag": null,
      "pageInfo": {"pageNumber": 0, "pageSize": 5},
      "flag": flag
    };
    if (flag == "user") {
      return this.http
        .post(this.serverURL + "/search/user", param_json, this.options)
        .map(out => out.json());
    }
    else if (flag == "store") {
      return this.http
        .post(this.serverURL + "/search/place", param_json, this.options)
        .map(out => out.json());
    }
  }
  clearSearch(userType){
      let param_json =   {
              "type" : userType
          }
      ;
      return this.http
          .post(this.serverURL + "/search/clearRecentSearch", param_json, this.options)
          .map(out => out.json());
  }

  profileUpdate(gender,occupatn,aboutYou, firstName, lastName, dob, maxDistance) {
    let param_json = {
      "id": this.userDetails.session.userId,
      "aboutYou": aboutYou,
      "firstName": firstName,
      "lastName": lastName,
      "dob": dob,
      "maxDistance": maxDistance,
        "sex" :gender,
        "work" : occupatn
    };
    return this.http
      .post(this.serverURL + "/user/update/myInfo", param_json, this.options)
      .map(out => out.json());
  }

  deactivateAcc() {
    let param_json = {};
    return this.http
      .post(this.serverURL + '/user/deactivate', param_json, this.options)
      .map(out => out.json());
  }

  fbaseRegister(token) {
    let param_json = {
      "token": token
    };
    return this.http
      .post(this.serverURL + '/fcmregistry/register', param_json, this.options)
      .map(out => out.json());
  }

  fbaseUnregister(token) {
    let param_json = {
      "token": token
    };
    return this.http
      .post(this.serverURL + '/fcmregistry/unregister', param_json, this.options)
      .map(out => out.json());
  }

  getCoordinates(location) {
    return this.http.get('https://maps.googleapis.com/maps/api/geocode/json?address=' + location + '&key=AIzaSyBZU0JSLTK9CIBwuZwux2Z7M7ZuD1NST5U').map(res => res);
  }


    getConversations(id){
    	let param_json = {};
	  	return this.http
			.post(this.serverURL + "/chat/getConversations", param_json, this.options)
			.map(out => out.json());
	}
	newChatUserList(id){
    	let param_json = {};
	  	return this.http
			.post(this.serverURL + "/chat/newChatUserList", param_json, this.options)
			.map(out => out.json());
	}
	getMessages(param_json={}){
	  	return this.http
			.post(this.serverURL + "/chat/getMessages", param_json, this.options)
			.map(out => out.json());
	}
	sendMessages(param_json={}){
	  	return this.http
			.post(this.serverURL + "/chat/sendMessages", param_json, this.options)
			.map(out => out.json());
	}
	/*addFile(param_json={}){
	  	return this.http
			.post(this.serverURL + "/chat/addFile", param_json, this.options)
			.map(out => out.json());
	}*/
	addFile(param_json={}){
		/*var headers;
		this.storage.get('userDetails').then((data) => {if(data){
        	headers = new Headers({
			    'Content-Type': 'multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW',
			    'token': data.session.token,
			    'userid': data.session.userId
			});
    	}});

		//TODO: remove
    	headers = new Headers({
		    //'Content-Type': 'multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW',
		    'token': '2915cd58-cb71-4a4f-8aab-f1329340617a',
		    'userid': '5a3e69af5fd6bf0c51e15dcf'
		});

    	let options = new RequestOptions({ headers: headers, withCredentials: true }); this.optionsMultipart*/
	    return this.http
			.post(this.serverURL + "/chat/addFile", param_json, this.options)
			.map(out => out.json());
	}

	markRead(userId){
	  	return this.http
			.post(this.serverURL + "/chat/markRead", {"userId": userId}, this.options)
			.map(out => out.json());
	}

	deleteConversation(param_json={}){
	  	return this.http
			.post(this.serverURL + "/chat/deleteConversation", param_json, this.options)
			.map(out => out.json());
	}

	getUnreadConversations(param_json={}){
	  	return this.http
			.post(this.serverURL + "/chat/getUnreadConversations", param_json, this.options)
			.map(out => out.json());
	}
    getUnreadNotification(param_json={}){
        return this.http
            .post(this.serverURL + "/notification/unreadCount", param_json, this.options)
            .map(out => out.json());
    }
    getRecentSearch(param_json={
                        "type" : "USER"
                    }){
	  console.log(this.options);
        return this.http
            .post(this.serverURL + "/search/getRecentSearch", param_json, this.options)
            .map(out => out.json());
    }
    getPlaceSearch(param_json={
        "type" : "STORE"
    }){
        return this.http
            .post(this.serverURL + "/search/getRecentSearch", param_json, this.options)
            .map(out => out.json());
    }
    addRecentSearch(myType,clickID)
    {
        return this.http
            .post(this.serverURL + "/search/addRecentSearch", {
                "type" : myType,"id" : clickID}, this.options)
            .map(out => out.json());
    }
  fcmRegister(token){
      return this.http
      .post(this.serverURL + "/fcmregistry/register", {"token": token}, this.options)
      .map(out => out.json());
  }

  fcmUnregister(token){
      return this.http
      .post(this.serverURL + "/fcmregistry/unregister", {"token": token}, this.options)
      .map(out => out.json());
  }

  fetchAllNotifications(param_json={}){
      return this.http
      .post(this.serverURL + "/notification/fetchall", param_json, this.options)
      .map(out => out.json());
  }
  ackNotifications(param_json={}){
      return this.http
      .post(this.serverURL + "/notification/ack", param_json, this.options)
      .map(out => out.json());
  }
  unreadNotificationCount(param_json={}){
      return this.http
      .post(this.serverURL + "/notification/unreadCount", param_json, this.options)
      .map(out => out.json());
  }
    signUp(mydata){
        this.signupInfo=mydata;
        let _headers = new Headers({
            'Content-Type':'application/json'
        });
        let options = new RequestOptions({
            headers: _headers,
        });
        return Observable.create(observer => {
            this.http.post(this.serverURL +"/auth/signup",mydata,options)
                .map(res => res.json())
                .subscribe(data => {
                    observer.next(data);
                    observer.complete();
                }, error => {
                    console.log(error);
                    observer.next(false);
                    observer.complete();
                }, () => {
                    console.log('view was not dismissed');
                });
        });
    }
    signupData() {
        return this.signupInfo;
    }
    manualLogin(mydata){
        this.loginInfo=mydata;
        let _headers = new Headers({
            'Content-Type':'application/json'
        });
        let options = new RequestOptions({
            headers: _headers,
        });
        return Observable.create(observer => {
            this.http.post(this.serverURL +"/auth/mLogin",mydata,options)
                .map(res => res.json())
                .subscribe(data => {
                    observer.next(data);
                    observer.complete();
                }, error => {
                    console.log(error);
                    observer.next(false);
                    observer.complete();
                }, () => {
                    console.log('view was not dismissed');
                });
        });
    }
    manualLoginData() {
        return this.loginInfo;
    }
    verifyaccount(mydata){
        let _headers = new Headers({
            'Content-Type':'application/json'
        });
        let options = new RequestOptions({
            headers: _headers,
        });
        return Observable.create(observer => {
            this.http.post(this.serverURL +"/auth/signup",mydata,options)
                .map(res => res.json())
                .subscribe(data => {
                    this.signupInfo=data;
                    observer.next(data);
                    observer.complete();
                }, error => {
                    console.log(error);
                    observer.next(false);
                    observer.complete();
                }, () => {
                    console.log('view was not dismissed');
                });
        });
    }
    public userloginfacebook(accesstoken) {
	  console.log("inside user login");
        console.log(accesstoken);
        let param_json = {
            "accessToken": accesstoken,
            "email": "",
            "socialAppType": 10,
        }
            return Observable.create(observer => {
                this.http.post(this.serverURL + '/auth/login',param_json,this.options)
                    .map(res => res.json())
                    .subscribe(data => {
                        console.log("inside login response ");
                        console.log(data);
                        this.testUserInfo=data;
                        localStorage.setItem('myuserToken',this.testUserInfo.session.token);
                        localStorage.setItem('myuserUserId',this.testUserInfo.session.userId);
                        console.log(this.testUserInfo);
                        observer.next(data);
                        observer.complete();
                    }, error => {
                        console.log(error);
                    }, () => {
                        console.log('view was not dismissed');
                    });
            });
        }
    loadConfig(){
	  let data={};
        let headers = new Headers({
            "content-type": "application/json",
        });
        let options = new RequestOptions({
            headers: headers,
        });
        return Observable.create(observer => {
            console.log("inside observable");
            this.http.post(this.serverURL +"/auth/getSystemInfo",data,options)
                .map(res => res.json())
                .subscribe(data => {
                    console.log("api response");
                    this.configData=data;
                    console.log(this.configData.systemInfo.versionId);
                    this.versionAppCompare();
                    observer.next(data);
                    observer.complete();

                }, error => {
                    observer.next(error);
                    observer.complete();
                });
        });
    };
    public versionAppCompare(){
        console.log(this.configData);
        if (this.platform.is('android')){
            this.appVersion.getVersionNumber().then(
                (response)=>{
                    console.log(response);
                    console.log(this.configData.systemInfo.versionId);
                    if(response < this.configData.systemInfo.versionId) {
                        let versionModal = this.modalCtrl.create('VersionPage');
                        versionModal.present();
                    };
                });
        }
        else  if (this.platform.is('ios')){
            this.appVersion.getVersionNumber().then(
                (response)=>{
                    console.log(response);
                    console.log(this.configData.systemInfo.versionId);
                    if(response < this.configData.systemInfo.iosVersionId) {
                        let versionModal = this.modalCtrl.create('VersionPage');
                        versionModal.present();
                    };
                });
        }
    }
        public getUserInfo(){
	      console.log(this.testUserInfo);
	      return this.testUserInfo;
        }

}
