import { NgModule } from '@angular/core';
import { PipesModulePipe } from './pipes-module/pipes-module';
import {NiceDateFormatPipe} from "../providers/global/niceDateFormatPipe";
import {FilterPipe} from "../providers/global/filter";
@NgModule({
	declarations: [PipesModulePipe,NiceDateFormatPipe,FilterPipe],
	imports: [],
	exports: [PipesModulePipe,NiceDateFormatPipe,FilterPipe]
})
export class PipesModule {}
