import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GlobalProvider } from '../../providers/global/global';

/**
 * Generated class for the PlacesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-places',
  templateUrl: 'places.html',
})
export class PlacesPage {

  options = 'people';
  cities = 'Noida';
  public sarchdata : any;
  public sarchFlag : any;

  constructor(public navCtrl: NavController,public globalProvider: GlobalProvider,
    public navParams: NavParams) {
    this.sarchFlag='user';
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PlacesPage');
  }


  navigateToPlace() {
    this.navCtrl.push('PlaceDetailPage');
  }

  navigateToPerson() {
    this.navCtrl.push('PersonDetailPage');
  }
  selectedPeople() {
    this.sarchFlag='user';
    console.log("selectedPeople");
  }
  selectedPlaces() {
    this.sarchFlag='place';
    console.log("selectedPlaces");
  }

  getItems(ev) {
    var val = ev.target.value;
     if(val.length > 1){
      this.Srachall(val);
    }
  }
  Srachall(res){
    this.globalProvider.searchProduct(res,this.sarchFlag)
        .subscribe(data => {
            this.sarchdata = data.userList;
            console.log(this.sarchdata);

       });
  }
}
