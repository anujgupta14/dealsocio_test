import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {Storage} from "@ionic/storage";
import {GlobalProvider} from "../../providers/global/global";
import 'rxjs/Rx';
/**
 * Generated class for the SearchPlacesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-search-places',
  templateUrl: 'search-places.html',
})
export class SearchPlacesPage {
    public recentPlace:any=[];
    public searchTerm: string = '';
    public storedata: any=[];
    public baseURL = this.globalProvider.baseURL;
    rootNavCtrl: NavController;
  constructor(public storage: Storage,public globalProvider: GlobalProvider,public navCtrl: NavController, public navParams: NavParams) {
      this.rootNavCtrl = navParams.get('rootNavCtrl');
  }
    ionViewDidEnter(){
     this.allmystore("");
    }
    allmystore(info){
        this.storage.get('userDetails').then((data) => {
            this.globalProvider.getPlaceSearch()
                .subscribe(data => {
                    if(info){
                        info.complete();
                    }
                    this.recentPlace=data.stores;
                    console.log(this.recentPlace);
                });
        });
    }
    getStores(ev) {
        var val = ev.target.value;
        this.searchTerm=ev.target.value;
        if(val==undefined){
            this.cancelPlace("");
        }else{
            if (val.length >= 1) {
                this.searchAllStores(val);
            }
            else{
                this.storedata = [];
            }
        }
    }
    cancelPlace(evn){
        this.searchTerm='';
        this.storedata = [];
    }
    searchAllStores(res) {
        this.globalProvider.searchProduct(res, 'store').debounceTime(2000).distinctUntilChanged()
            .subscribe(data => {
                this.storedata = data;
                console.log(this.storedata);
            });
    }
    clearSearch(typeSearch){
        this.globalProvider.clearSearch(typeSearch)
            .subscribe(data => {
                if(data.successful){
                        this.recentPlace=[];
                }
            });
    }
    getPlaceInfo(exp){
      console.log(exp);
        this.globalProvider.makebackActive=true;
        this.rootNavCtrl.push('PlaceDetailPage', {
            "id" : exp
        });
    }
    addinRecent(myType,clickedId){
        this.globalProvider.addRecentSearch(myType,clickedId)
            .subscribe(data => {
                console.log("get recent serach");
                console.log(data);
            });
    }
}
