import {
	animate,
	Component,
	state,
	style,
	transition,
	trigger,
	ViewChild
} from "@angular/core";
import {
	AlertController,
	Content,
	Events,
	IonicPage,
	NavController,
	NavParams,
	ToastController
} from "ionic-angular";
import { LoadingProvider } from "../../providers/loading/loading";
import { GlobalProvider } from "../../providers/global/global";
import { Network } from "@ionic-native/network";

/**
 * Generated class for the MyeventsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
	selector: "page-myevents",
	templateUrl: "myevents.html",
	animations: [
		trigger("listItemState", [
			state(
				"in",
				style({
					opacity: 1,
					height: "*",
					minHeight: "*"
				})
			),
			transition("* => void", [
				animate(
					"0.25s",
					style({
						opacity: 0,
						height: "1px",
						minHeight: "1px",
						transform: "perspective(500px) translateZ(-400px) scale(1.2)"
					})
				)
			])
		])
	]
})
export class MyeventsPage {
	@ViewChild("pageTop") pageTop: Content;
	public my_events: any = [];
	public pageNo: number = 0;
	public removeCount: number = 0;
	public baseURL = this.globalProvider.baseURL;
	showAccord1: Array<boolean> = [];
	showAccord2: Array<boolean> = [];
	rootNavCtrl: NavController;
	noapicall: boolean = true;
	constructor(
		private alertCtrl: AlertController,
		public events: Events,
		private toastCtrl: ToastController,
		public loadingService: LoadingProvider,
		private network: Network,
		public globalProvider: GlobalProvider,
		public navCtrl: NavController,
		public navParams: NavParams
	) {
		this.rootNavCtrl = navParams.get("rootNavCtrl");
		console.log("constructor");
		this.events.subscribe("user:event", data => {
			this.getEventsList("");
		});
	}

	ionViewDidEnter() {
		this.globalProvider.makebackActive = false;
		console.log("ionViewDidLoad MyeventsPage");
		this.getEventsList("");
	}
	getEventsList(info) {
		this.loadingService.showLoading("Getting My Events");
		//this.pageTop.scrollToTop();
		this.pageNo = 0;
		this.globalProvider.getmyflyers(this.pageNo).subscribe(data => {
			console.log(data);
			if (info) {
				info.complete();
			}
			this.loadingService.hideLoading();
			this.my_events = data.flyerDtos;
			if (this.my_events.flyers) {
				this.showAccord2 = Array(this.my_events.flyers.length).fill(false);
			}
		});
	}
	presentConfirm(id, index) {
		let alert = this.alertCtrl.create({
			title: "",
			message: "Are You Sure?",
			buttons: [
				{
					text: "No",
					role: "cancel",
					handler: () => {
						console.log("Cancel clicked");
					}
				},
				{
					text: "Yes",
					handler: () => {
						this.disable(id, index);
					}
				}
			]
		});
		alert.present();
	}
	disable(fid, index) {
		++this.removeCount;
		this.my_events.splice(index, 1);
		this.globalProvider.disableflyer(fid).subscribe(data => {
			console.log(data);
			// if(this.removeCount>0 && this.removeCount%10==0){
			//     this.getData("");
			// }
		});
		if (this.my_events.length == 1) {
			this.getData("");
		}
	}
	getData(info) {
		if (this.network.type != "none") {
			++this.pageNo;
			console.log(this.pageNo);
			this.globalProvider.getmyflyers(this.pageNo).subscribe(response => {
				if (info) {
					info.complete();
				}
				//this.loading.dismiss();
				console.log(response);
				if (response.flyerDtos.length > 0) {
					for (var i = 0; i < response.flyerDtos.length; i++) {
						this.my_events.push(response.flyerDtos[i]);
					}
				} else {
					if (
						response.message == "NO EVENT AT THIS MOMENT" &&
						this.pageNo > 0
					) {
						this.noapicall = false;
						this.loadingService.showNotification(
							"No More Events to show",
							"right",
							""
						);
					}
					//this.nomoreData=true;
				}
			});
		} else {
			if (info) {
				info.complete();
			}
			let toast = this.toastCtrl.create({
				message: "check your internet connection before process",
				duration: 2000,
				position: "middle"
			});
			toast.present();
		}
	}
	goToGoogleMap(mapData) {
		window.open(
			"geo:0,0?q=" + mapData.coordinates[1] + "," + mapData.coordinates[0],
			"_system"
		);
	}
	getInterestedList(Fid) {
		this.globalProvider.makebackActive = true;
		this.rootNavCtrl.push("FlyerreqPage", {
			flyerId: Fid
		});
	}
	getAccepted(Fid) {
		this.globalProvider.makebackActive = true;
		this.rootNavCtrl.push("UserListPage", {
			flyerId: Fid
		});
	}
}
