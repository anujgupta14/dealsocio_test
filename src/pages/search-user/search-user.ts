import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {Storage} from "@ionic/storage";
import {GlobalProvider} from "../../providers/global/global";

/**
 * Generated class for the SearchUserPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-search-user',
  templateUrl: 'search-user.html',
})
export class SearchUserPage {
    public recentUser:any=[];
    public userTerm: string = '';
    public userdata: any=[];
    public baseURL = this.globalProvider.baseURL;
    rootNavCtrl: NavController;
  constructor(public storage: Storage,public globalProvider: GlobalProvider,public navCtrl: NavController, public navParams: NavParams) {
      this.rootNavCtrl = navParams.get('rootNavCtrl');
  }

    ionViewDidEnter(){
        this.allmyUser("");

    }
    allmyUser(info){
        this.storage.get('userDetails').then((data) => {
            this.globalProvider.getRecentSearch()
                .subscribe(data => {
                    if(info){
                        info.complete();
                    }
                    this.recentUser=data.users;
                    console.log("** searched data users **");
                    console.log(this.recentUser);
                });
        });
    }
    getItems(ev) {
        var val = ev.target.value;
        if(val==undefined){
            this.cancelItem("");
        }else {
            if (val.length >= 1) {
                this.searchAllUsers(val);
            }
            else {
                this.userdata = [];
            }
        }
    }
    searchAllUsers(res) {
        this.globalProvider.searchProduct(res, 'user').debounceTime(2000).distinctUntilChanged()
            .subscribe(data => {
                console.log("inside user");
                this.userdata = data.userList;
            });
    }
    cancelItem(evn){
        this.userTerm='';
        this.userdata = [];
    }
    getUserProfileInfo(id){
        this.globalProvider.makebackActive=true;
        this.rootNavCtrl.push('UserDetailPage', {
            userid : id
        });
    }
    addinRecent(myType,clickedId){
        this.globalProvider.addRecentSearch(myType,clickedId)
            .subscribe(data => {
                console.log("get recent serach");
                console.log(data);
            });
    }
    clearSearch(typeSearch){
        this.globalProvider.clearSearch(typeSearch)
            .subscribe(data => {
                if(data.successful){
                        this.recentUser=[];
                }

            });
    }
}
