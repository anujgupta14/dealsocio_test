import { Component } from "@angular/core";
import {
	IonicPage,
	NavController,
	NavParams,
	ToastController
} from "ionic-angular";
import { GlobalProvider } from "../../providers/global/global";
import { GoogleMaps } from "../../providers/google-maps/google-maps";
import { DatePicker } from "@ionic-native/date-picker";
import { LoadingProvider } from "../../providers/loading/loading";
//import { NativeGeocoder, NativeGeocoderReverseResult, NativeGeocoderForwardResult } from '@ionic-native/native-geocoder';
/**
 * Generated class for the CreatePlanPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
declare var google: any;
declare var moment: any;

@IonicPage()
@Component({
	selector: "page-create-plan",
	templateUrl: "create-plan.html"
})
export class CreatePlanPage {
	public store: any;
	public offerId: any;
	public publicFlag: boolean;
	public isDescription: boolean;
	query: string = "";
	disableButton: boolean = false;
	nameLocation: string = "";
	places: any = [];
	autocompleteService: any;
	public allExperiances: any;
	public expireName: any;
	public expireID: any;
	public selectLat: any;
	public selectLong: any;
	public mydate: any;
	public storeInfo: any;
	public nameStore: any = "";
	loginData = { expireDate: 0, selectCity: "", description: "" };
	constructor(
		private datePicker: DatePicker,
		public loadingService: LoadingProvider,
		public navCtrl: NavController,
		private toastCtrl: ToastController,
		public navParams: NavParams,
		public globalProvider: GlobalProvider,
		public maps: GoogleMaps
	) {
		let todayDate = new Date();
		console.log(new Date());
		console.log(new Date().toISOString());
		let dd = todayDate.getDate();
		let mm = todayDate.getMonth() + 1; //January is 0!
		let yyyy = todayDate.getFullYear();

		//this.checkInDate = todayDate.toISOString();
		console.log(mm + "/" + dd + "/" + yyyy);
		this.mydate = mm + "/" + dd + "/" + yyyy;
		// this.currYear = yyyy;
		//alert(this.navParams.data.store);
		console.log(this.navParams.data.store.storeName);

		if (this.navParams.data.store != " ") {
			console.log(this.navParams.data.store.location.coordinates[0]);
			console.log(this.navParams.data.store.location.coordinates[1]);
			this.selectLat = this.navParams.data.store.location.coordinates[1];
			this.selectLong = this.navParams.data.store.location.coordinates[0];
			this.nameStore = this.navParams.data.store.storeName;
		}
		this.storeInfo = this.navParams.data.store;
		this.nameLocation = this.storeInfo.locality;

		this.offerId = "";
		this.allExperiances = this.globalProvider.experlist;
		this.publicFlag = true;
		this.isDescription = false;
		this.expireName = this.navParams.data.experianceName;
		//this.expireName=this.globalProvider.experiance.name;
		if(this.globalProvider.experiance){
            this.expireID = this.globalProvider.experiance.id;
		}else {

		}

	}

	dates;
	ionViewDidLoad() {
		console.log("ionViewDidLoad CreatePlanPage");
		this.autocompleteService = new google.maps.places.AutocompleteService();
		this.store = this.navParams.get("store");
		if (document.getElementById("input")) {
			if (
				document.getElementById("input").getElementsByTagName("INPUT") != null
			) {
				const inputs: any = document
					.getElementById("input")
					.getElementsByTagName("INPUT");
				inputs[0].disabled = true;
			}
		}

		let dateSelector = document.getElementById("selectDate");
	}

	close() {
		this.navCtrl.pop();
	}
	createFlyer() {
		if(this.navParams.get("experiance")){
            if(this.navParams.get("experiance").id){
                console.log("inside true");
                console.log(this.navParams.get("experiance").id);
                this.expireID=this.navParams.get("experiance").id
            }
		}
		else{
            console.log("inside false");
			console.log(this.navParams.get("store"));
           // this.store.id = this.navParams.get("store").id;
		}
		if (!this.selectLat || !this.selectLat) {
			let toast = this.toastCtrl.create({
				message: "Location is mandatory",
				duration: 1000,
				position: "middle"
			});
			toast.onDidDismiss(() => {
				console.log("Dismissed toast");
			});
			toast.present();
		} else if (this.loginData.expireDate == 0) {
			let toast = this.toastCtrl.create({
				message: "Date is mandatory",
				duration: 1000,
				position: "middle"
			});
			toast.onDidDismiss(() => {
				console.log("Dismissed toast");
			});
			toast.present();
		} else if (!this.nameStore) {
			let toast = this.toastCtrl.create({
				message: "Name is mandatory",
				duration: 1000,
				position: "middle"
			});
			toast.onDidDismiss(() => {
				console.log("Dismissed toast");
			});
			toast.present();
		} else {
			console.log("inside else");
			this.disableButton = true;
			this.loadingService.showLoading("creating");
			console.log(this.navParams.get("experiance"));
			this.globalProvider
				.createFlyer(
					this.store.id,
					this.offerId,
					this.publicFlag,
					this.selectLat,
					this.selectLong,
					this.loginData.expireDate,
					this.expireID,
					this.loginData.description,
					this.nameStore
				)
				.subscribe(data => {
					this.loadingService.hideLoading();
					//this.sarchdata = data;
					console.log(data);
					if (data.successful) {
						let toast = this.toastCtrl.create({
							message: data.message,
							duration: 1000,
							position: "bottom"
						});
						toast.onDidDismiss(() => {
							console.log("Dismissed toast");
							this.disableButton = false;
							if(this.store){
                                this.navCtrl.popTo(this.navCtrl.getByIndex(this.navCtrl.length() - 4));
							}else{
                                this.navCtrl.pop();
							}

						});
						toast.present();
					} else {
						let toast = this.toastCtrl.create({
							message: data.message,
							duration: 1000,
							position: "bottom"
						});
						toast.onDidDismiss(() => {
							this.disableButton = false;
							console.log("Dismissed toast");
						});
						toast.present();
					}
				});
		}
	}
	changeDate(mydate) {
		this.datePicker
			.show({
				date: new Date(),
				mode: "date",
				minDate: this.mydate,
				androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK
			})
			.then(
				date => {
					console.log("Got date: ", this.selectedDate(date));
					const newDate = moment(date).format("ddd DD/MM/YYYY");
					this.dates = newDate.toString();
				},
				err => console.log("Error occurred while getting date: ", err)
			);
	}
	selectedDate(date) {
		var cdate = new Date(date);
		this.loginData.expireDate = cdate.getTime();
	}
	onPublicFlag(flag) {
		this.publicFlag = flag;
	}
	onDescriptionChange() {
		if (this.isDescription) {
			this.globalProvider.getmyprof().subscribe(data => {
				this.loginData.description = data.user.aboutYou;
			});
		}
	}

	searchPlace() {
		if (this.query.length > 0) {
			let config = {
				//types: ['geocode'],
				input: this.query,
				componentRestrictions: { country: "in" }
			};
			this.autocompleteService.getPlacePredictions(
				config,
				(predictions, status) => {
					if (
						status == google.maps.places.PlacesServiceStatus.OK &&
						predictions
					) {
						this.places = [];
						predictions.forEach(prediction => {
							this.places.push(prediction);
						});
					}
				}
			);
		} else {
			this.places = [];
		}
	}

	logplace(location) {
		this.globalProvider.getCoordinates(location.description).subscribe(res => {
			let data = res.json();
			let latLong = data["results"][0]["geometry"]["location"];
			this.query = location.description;
			this.places = [];
			console.log(latLong);
			this.selectLat = latLong.lat;
			this.selectLong = latLong.lng;
		});
	}
}
