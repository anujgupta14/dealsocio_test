import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CreatePlanPage } from './create-plan';
import {DatePicker} from "@ionic-native/date-picker";

@NgModule({
  declarations: [
    CreatePlanPage,
  ],
  imports: [
    IonicPageModule.forChild(CreatePlanPage),
  ],
    providers: [
      DatePicker
    ]
})
export class CreatePlanPageModule {}
