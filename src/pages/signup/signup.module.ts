import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SignupPage } from './signup';
import {DatePicker} from "@ionic-native/date-picker";

@NgModule({
  declarations: [
    SignupPage,
  ],
  imports: [
    IonicPageModule.forChild(SignupPage),
  ],
    providers: [
        DatePicker
    ]
})
export class SignupPageModule {}
