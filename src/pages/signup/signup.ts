import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {DatePicker} from "@ionic-native/date-picker";
import {LoadingProvider} from "../../providers/loading/loading";
import {GlobalProvider} from "../../providers/global/global";

/**
 * Generated class for the SignupPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
declare var moment: any;
@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class SignupPage {
    myForm: FormGroup;
    public mydate: any;
    public dates:any;
    public mydob:any='953310469';
  constructor(public loadingService:LoadingProvider,public globalProvider: GlobalProvider, private datePicker: DatePicker,public formBuilder: FormBuilder,public navCtrl: NavController, public navParams: NavParams) {
      this.myForm =  this.formBuilder.group({
          'firstname': ['', Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(15)])],
          'lastname': ["", Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(15)])],
          'email': ["", Validators.compose([Validators.required, Validators.pattern('^[A-Za-z0-9]+(\.[_A-Za-z0-9]+)*@[A-Za-z0-9-]+(\.[a-z0-9-]+)(\.[a-z]{2,15})$')])],
          'phone': ['', Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(10)])],
          'dob': '',
          'gender': 'male',
      });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SignupPage');
  }
    onTest(formdata){
        let myformValue=
            {
                "firstName" : formdata.value.firstname,               // mandatory
                "lastName" : formdata.value.lastname,                     // mandatory
                "gender" : formdata.value.gender,                       // mandatory
                "email" : formdata.value.email ,    // not mandatory
                "dob" : this.mydob,                // mandatory
                "phoneNo" : formdata.value.phone,          // mandatory
                "isdCode" : "91",                         // mandatory
                "otp" : ""                              // not mandatory
            };
        console.log(myformValue);
        this.loadingService.showLoading("Authenticating...");
        this.globalProvider.signUp(myformValue).subscribe(
            (response)=>{
                this.loadingService.hideLoading();
                        if(response.successful){
                            this.navCtrl.push("VerifyAccountPage");
                        }else{
                            this.loadingService.showNotification(response.message,"wrong","");
                        }
            });

    }

    changeDate(mydate){
        this.datePicker.show({
            date: new Date(),
            mode: 'date',
            minDate: this.mydate,
            androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK
        }).then(
            date => {
                console.log('Test Got date: ', date);
                var cdate = new Date(date);
                console.log('selectedDate : '+ cdate);
                console.log('selectedDate : '+ cdate.getTime());
                this.mydob=cdate.getTime();
           //     console.log('Got date: ', this.selectedDate(date));
                const newDate = moment(date).format('MM/DD/YYYY');
                this.dates = newDate.toString();
                this.myForm.controls['dob'].setValue(this.dates);
            }
            ,
            err => console.log('Error occurred while getting date: ', err)
        );
    }
    // selectedDate(date) {
    //     console.log(new Date(date));
    //     console.log(new Date(date).toISOString());
    //     let dd = new Date(date).getDate();
    //     let mm = new Date(date).getMonth()+1; //January is 0!
    //     let yyyy = new Date(date).getFullYear();
    //     console.log(mm+"/"+dd+"/"+yyyy);
    //     var cdate = new Date(date);
    //     console.log('selectedDate : '+ cdate);
    //     console.log('selectedDate : '+ cdate.getTime());
    //    // this.loginData.expireDate=cdate.getTime();
    // }
}
