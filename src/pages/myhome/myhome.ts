import {Platform,IonicPage,NavController, NavParams, ToastController} from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { GlobalProvider } from '../../providers/global/global';
import {LoadingProvider} from "../../providers/loading/loading";
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import { Diagnostic } from '@ionic-native/diagnostic';
import {animate, Component, state, style, transition, trigger} from "@angular/core";
import {AndroidPermissions} from "@ionic-native/android-permissions";
import {LoginPage} from "../login/login";
import {Storage} from "@ionic/storage";
import { OpenNativeSettings } from '@ionic-native/open-native-settings';
import {Facebook} from "@ionic-native/facebook";
//import {Facebook} from "@ionic-native/facebook";
/**
 * Generated class for the MyhomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-myhome',
  templateUrl: 'myhome.html',
    animations: [
        trigger('listItemState', [
            state('in',
                style({
                    opacity: 1,
                    height:'*',
                    minHeight: '*'
                })
            ),
            transition('* => void', [
                animate('0.25s', style({
                    opacity: 0,
                    height: '1px',
                    minHeight: '1px',
                    transform: 'perspective(500px) translateZ(-400px) scale(1.2)',
                }))
            ])
        ])
    ]
})


export class MyhomePage {
    cards: Array<any>;
    recentCard: string = '';
    isOpen = true;
    slDirection :boolean;
  public userinfo = this.globalProvider.userDetails;
  public locationdata: any;
  public exp: any;
  public pageNo:number=0;
 // public approved:boolean=true;
 // public metadata: any=[];
  //public reversecount:any=5;
  public showdata:any=[];
  public cityname:string ="";
  public baseURL = this.globalProvider.baseURL;
  public gpsenable :boolean;
  public gpsPermission:boolean;
  public unreadCount: number = 0;
  public unreadNotify: number = 0;
  //public options:any;
   // mycount:number=0;
    noapicall:boolean=true;
  constructor(private openNativeSettings: OpenNativeSettings,private fb: Facebook,public storage: Storage,private androidPermissions: AndroidPermissions,private diagnostic: Diagnostic,private locationAccuracy: LocationAccuracy,public platform: Platform,public navCtrl: NavController,public loadingService:LoadingProvider, private toastCtrl: ToastController,public navParams: NavParams,private geolocation: Geolocation,public globalProvider: GlobalProvider) {
    this.cards=[];
      //this.enableLocation();
      this.otherFunction();
  }
  public otherFunction(){
      //getLocationAuthorizationStatus
      //isLocationAvailable
      //denied (location off (never)
      //authorized_when_in_use (on)  need data else

      this.diagnostic.getLocationAuthorizationStatus().then((isEnabled) => {
          if(isEnabled == "denied" && this.platform.is('ios')){
              let toast = this.toastCtrl.create({
                  message: "Please Turn On  Mobile App Location to access Page",
                  duration: 3000,
                  position: 'top'
              });
              toast.onDidDismiss(() => {
                  //this.diagnostic.switchToLocationSettings();
                  //this.getLocationCoordinates();
                  if (this.platform.is('ios')) {
                      this.openNativeSettings.open("application_details").then(val => {
                          console.log('opened settings');
                      }).catch(err => {
                          console.log(JSON.stringify(err));
                      });
                  }else{
                      this.getLocationCoordinates();
                  }
              });
              toast.present();
              //handle confirmation window code here and then call switchToLocationSettings
              //this.diagnostic.switchToLocationSettings();
          }
          else{
              this.getLocationCoordinates();
          }
      })
  }
// public enableLocation(){
//     this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION).then(
//         result => {
//             console.log(result);
//             if (result.hasPermission) {
//
//                 //If having permission show 'Turn On GPS' dialogue
//                 this.askToTurnOnGPS();
//             } else {
//
//                 //If not having permission ask for permission
//                 this.requestGPSPermission();
//             }
//         },
//         err => {
//             alert(err);
//         }
//     );
// }
//     public requestGPSPermission() {
//         this.locationAccuracy.canRequest().then((canRequest: boolean) => {
//             if (canRequest) {
//                 console.log("4");
//                 this.getallData("");
//             } else {
//                 //Show 'GPS Permission Request' dialogue
//                 //this.askToTurnOnGPS();
//                 this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION)
//                     .then(
//                         () => {
//                             // call method to turn on GPS
//                             this.askToTurnOnGPS();
//                         },
//                         error => {
//                             //Show alert if user click on 'No Thanks'
//                             this.platform.exitApp();
//                           //  alert('requestPermission Error requesting location permissions ' + error)
//                         }
//                     );
//             }
//         });
//     }
    askToTurnOnGPS() {
               this.diagnostic.isLocationEnabled().then( (available) => {
                   console.log("location is"+ available);
                   if(available){
                       this.getLocationCoordinates();
                   }else {
                       let toast = this.toastCtrl.create({
                           message: "Enable Mobile App Location to access Page",
                           duration: 3000,
                           position: 'middle'
                       });
                       toast.onDidDismiss(() => {
                           // this.getLocationCoordinates();
                           if (this.platform.is('ios')) {
                               this.openNativeSettings.open("application_details").then(val => {
                                   console.log('opened settings');
                               }).catch(err => {
                                   console.log(JSON.stringify(err));
                               });
                           }else{
                               this.getLocationCoordinates();
                           }
                       });
                       toast.present();
                   }
               });


    }
    getLocationCoordinates() {
      console.log("inside location coordinate");
        if(window.location.protocol == "https:" && this.geolocation){
            this.geolocation.getCurrentPosition().then((resp) => {
                console.log(resp);
                this.getallData("");
            },(err)=>{
                console.log("location error"+err);
            }).catch((error) => {
                console.log("location is not enabled")
                alert('Error getting location' + error);
            });
        }else{
                    this.locationAccuracy.canRequest().then((canRequest: boolean) => {
                        if (canRequest) {
// the accuracy option will be ignored by iOS
                            this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
                                () => this.getallData(""),
                                error => this.platform.exitApp()
                            );
                        } else {
                            console.log("inside error");
                            if (this.platform.is('ios'))
                            {
                                this.geolocation.getCurrentPosition().then((resp) => {
                                    console.log(resp);
                                    this.getallData("");
                                }, (err) => {
                                    console.log("location error" + err);
                                }).catch((error) => {
                                    console.log("location is not enabled")
                                    alert('Error getting location' + error);
                                });
                            }
                            // this.diagnostic.isLocationAuthorized().then(function (available) {
                            //     alert("Location is " + (available ? "available 2" : "not available 2"));
                            // }).catch(function (error) {
                            //     alert("The following error occurred: " + error);
                            // });
                        }
                    },(err)=>{
                        console.log("can request error"+err);
                    }).catch((error) => {
                        console.log("location is not enabled")
                        alert('Error getting can request' + error);
                    });
        }

    }
    public Report(userid,flyerId){
        this.navCtrl.push('ReasonPage',{'id':userid,'flyerid':flyerId})
    }
    // ngAfterViewInit() {
    //     // ViewChild & ViewChildren are only available
    //     // in this function
    //
    //     console.log(this.swingStack); // this is the stack
    //     console.log(this.swingCards); // this is a list of cards
    //
    //     // we can get the underlying stack
    //     // which has methods - createCard, destroyCard, getCard etc
    //     console.log(this.swingStack.stack);
    //
    //     // and the cards
    //     // every card has methods - destroy, throwIn, throwOut etc
    //     this.swingCards.forEach((c) => console.log(c.getCard()));
    //
    //     // this is how you can manually hook up to the
    //     // events instead of providing the event method in the template
    //     this.swingStack.throwoutleft.subscribe(
    //         (event: ThrowEvent) => console.log('Manual hook: ', event));
    //
    //     this.swingStack.dragstart.subscribe((event: DragEvent) => console.log());
    //
    //     this.swingStack.dragmove.subscribe((event: DragEvent) => console.log());
    // }
    // Called whenever we drag an element
    // onItemMove(element, x, y, r) {
    //   console.log("itemmove");
    //     var color = '';
    //     var abs = Math.abs(x);
    //     let min = Math.trunc(Math.min(16*16 - abs, 16*16));
    //    // console.log(min);
    //     let hexCode = this.decimalToHex(min, 2);
    //
    //     if (x < 0) {
    //         color = '#FF' + hexCode + hexCode;
    //     } else {
    //         color = '#' + hexCode + 'FF' + hexCode;
    //     }
    //
    //     element.style.background = color;
    //     element.style['transform'] = `translate3d(0, 0, 0) translate(${x}px, ${y}px) rotate(${r}deg)`;
    // }

    // checkCount(X,Y,R){
    //     ++this.mycount;
    //     console.log(this.swingStack);
    //     console.log(this.swingStack); // this is the stack
    //     console.log(this.swingCards); // this is a list of cards
    //
    //     // we can get the underlying stack
    //     // which has methods - createCard, destroyCard, getCard etc
    //     console.log(this.swingStack.stack);
    //   console.log(this.cards);
    //   console.log(this.cards[this.cards.length - 1]);
    //     var x=X;
    //     var y=Y;
    //     var r=R;
    //     console.log(x);
    //     console.log(y);
    //     console.log(r);
    //     var color = '';
    //     var abs = Math.abs(x);
    //     let min = Math.trunc(Math.min(16*16 - abs, 16*16));
    //     let hexCode = this.decimalToHex(min, 2);
    //
    //     if (x < 0) {
    //         color = '#FF' + hexCode + hexCode;
    //     } else {
    //         color = '#' + hexCode + 'FF' + hexCode;
    //     }
    //    // console.log(document.getElementById("mycards"+(this.reversecount-1)));
    //     // document.getElementById("mycards1").style.background = color;
    //     // document.getElementById("mycards1").style.transitionTimingFunction = "ease-in";
    //     const card = document.getElementById("mycards1");
    //     const test = $('#mycards1 ion-card.mytestclass:last');
    //     console.log(test[0]);
    //     card.style['z-index'] = 999;
    //     card.style['transform'] = `translate3d(0, 0, 0) translate(${x}px, ${y}px) rotate(${r}deg)`;
    //     card.style['background'] = color;
    //     setTimeout( () => {
    //         card.style['transform'] = `translate3d(0, 0, 0) translate(-500px, ${y}px) rotate(${r}deg)`;
    //         card.style['background'] = 'white';
    //         }, 1000);
    //     --this.reversecount;
    //     this.cards.pop();
    //     console.log(this.mycount);
    //     if(this.mycount>0 && this.mycount%4==0){
    //         ++this.pageNo;
    //         this.getnewData();
    //     }
    // }
    // http://stackoverflow.com/questions/57803/how-to-convert-decimal-to-hex-in-javascript
    // decimalToHex(d, padding) {
    //     var hex = Number(d).toString(16);
    //     padding = typeof (padding) === "undefined" || padding === null ? padding = 2 : padding;
    //
    //     while (hex.length < padding) {
    //         hex = "0" + hex;
    //     }
    //
    //     return hex;
    // }

//         enableLocation()
//         {
//             console.log("inside enable location ");
//             this.locationAccuracy.canRequest().then((canRequest: boolean) => {
// console.log(canRequest);
//                 if(canRequest) {
//                     console.log("inside can request");
//                 // the accuracy option will be ignored by iOS
//                     this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
//                         () => this.getallData(""),
//                         error => this.platform.exitApp()
//                     );
//                 }else{
//                     console.log("inside else request");
//                     this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
//                         () => this.getallData(""),
//                         error => this.platform.exitApp()
//                     );
//                 }
//
//             }), this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY ;
//         }
//     enableLocation()
//     {
//         this.diagnostic.isLocationAuthorized().then(function (available) {
//             alert("Location is " + (available ? "available 1" : "not available1 "));
//         }).catch(function (error) {
//             alert("The following error occurred: " + error);
//         });
//         this.diagnostic.isLocationEnabled().then(function (available) {
//             alert("Location is " + (available ? "enabled" : "not enabled"));
//             if(available==false){
//                 this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
//                     () => this.getallData(""),
//                     error => console.log('Error requesting location permissions', JSON.stringify(error))
//                 );
//             }
//         }).catch(function (error) {
//             alert("The following error occurred: " + error);
//         });
//         this.locationAccuracy.canRequest().then((canRequest: boolean) => {
//             console.log(canRequest);
//             if(canRequest) {
// // the accuracy option will be ignored by iOS
// //                 this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
// //                     () => this.getallData(""),
// //                     error => console.log('Error requesting location permissions', JSON.stringify(error))
// //                 );
//             }else{
//                 console.log("inside error");
//                 this.diagnostic.isLocationAuthorized().then(function (available) {
//                     alert("Location is " + (available ? "available 2" : "not available 2"));
//                 }).catch(function (error) {
//                     alert("The following error occurred: " + error);
//                 });
//                 this.geolocation.getCurrentPosition().then((resp) => {
//                     console.log(resp);
//                     // this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
//                     //     () => this.getallData(""),
//                     //     error => console.log('Error requesting location permissions', JSON.stringify(error))
//                     // );
//                     }
//                 )
//             }
//         });
//     }
//     open(){
//
//
//         this.locationAccuracy.canRequest().then((canRequest: boolean) => {
//             console.log(canRequest);
//             this.gpsPermission=canRequest;
//             if(canRequest) {
//                 // the accuracy option will be ignored by iOS
//                 this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
//                     () => console.log('Request successful'),
//                     error => console.log('Error requesting location permissions', error)
//                 );
//             }else{
//                 //
//
//                 let toast = this.toastCtrl.create({
//                     message: "Dealsocio Requires Location Permission",
//                     duration: 4000,
//                     position: 'top'
//                 });
//                 toast.onDidDismiss(() => {
//                    // this.loadData();
//                     //this.platform.exitApp();
//                   //  this.diagnostic.switchToLocationSettings();
//                 });
//                 toast.present();
//             }
//
//         });
//     }

    ionViewDidLoad(){
    //  this.getallData();

        // Either subscribe in controller or set in HTML
        // this.swingStack.throwin.subscribe((event: DragEvent) => {
        //     event.target.style.background = '#ffffff';
        // });

        this.cards = [];
       // this.addNewCards(0);
    }
    loadData(){
        console.log(this.gpsenable);
        console.log(this.gpsPermission);
        if(this.gpsenable==false){
            this.locationAccuracy.canRequest().then((canRequest: boolean) => {

                if(canRequest) {
                    // the accuracy option will be ignored by iOS
                    this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
                        () => console.log('Request successful'),
                        error => console.log('Error requesting location permissions', error)
                    );
                }

            });
            // let toast = this.toastCtrl.create({
            //     message: "please check GPS and Relaunch APP",
            //     duration: 3000,
            //     position: 'top'
            // });
            // toast.onDidDismiss(() => {
            //     this.platform.exitApp();
            // });
            // toast.present();
        }
        if(!this.gpsPermission){
            let toast = this.toastCtrl.create({
                message: "please check App Permission and Relaunch APP",
                duration: 3000,
                position: 'top'
            });
            toast.onDidDismiss(() => {
                this.platform.exitApp();
            });
            toast.present();
        }
    }

    ionViewWillEnter(){
        console.log("********** in view Enter function *****");
    console.log('notificationRedirectPage: ', this.globalProvider.notificationRedirect.page);
    if(this.globalProvider.notificationRedirect.page != ""){
     //this.navCtrl.push(TabsPage);
   //  this.navCtrl.push(this.globalProvider.notificationRedirect.page , this.globalProvider.notificationRedirect.data);

      this.globalProvider.notificationRedirect = {page:"", data:{}};
    }else{
      //this.getallData();
    }

    this.globalProvider.getUnreadConversations()
    .subscribe(data => {
      this.unreadCount = data.count;
    });
        this.globalProvider.getUnreadNotification()
            .subscribe(data => {
                if(data.successful){
                    this.unreadNotify=data.count;
                }
            });
  }



    public getallData(info){
      this.pageNo=0;
        this.cards=[];
        this.noapicall=true;
        this.loadingService.showLoading("Getting Events");
        this.geolocation.getCurrentPosition().then((resp) => {
            this.locationdata = resp;
            localStorage.setItem('myLatitude',this.locationdata.coords.latitude);
            localStorage.setItem('myLongitude',this.locationdata.coords.longitude);
            let storeId = this.navParams.get('storeId');
            this.globalProvider.getflyerfeed(this.locationdata.coords.latitude, this.locationdata.coords.longitude,/*28.6315, 77.2167,*/ storeId,this.pageNo)
                .subscribe(result => {
                    console.log(result);
                    console.log(result.successful);
                    console.log(result.status);
                    if(info){
                        info.complete();
                    }
                    if(result.successful==false){
                        if(result.status=="401"){
                            let toast = this.toastCtrl.create({
                                message: result.message,
                                duration: 3000,
                                cssClass: "wrong",
                                position: 'bottom'
                            });
                            toast.onDidDismiss(() => {
                                this.storage.clear();
                                this.fb.logout();
                                localStorage.clear();
                                this.navCtrl.setRoot(LoginPage);
                            });
                            toast.present();
                        }
                    }else {
                    this.loadingService.hideLoading();
                    for (let myval of result.flyerDtoList) {
                        this.cards.push(myval);
                        console.log(this.cards);
                    }
                    if(result.message== "NO EVENTS AT THIS MOMENT" && this.pageNo>0){
                        this.noapicall=false;
                        this.loadingService.showNotification("No More Events to show","right","");
                    }
                   else if(result.status==404){
                        this.loadingService.showNotification(result.message,"wrong","");
                    }else if(result.status==500) {
                        var a = JSON.parse(result._body);
                        this.loadingService.showNotification(a.message, "wrong", "");
                    }
                    // else{
                    //     this.loadingService.showNotification(result.message,"wrong","");
                    // }
                    }
                });
            this.globalProvider.myLocation(this.locationdata.coords.longitude, this.locationdata.coords.latitude)
                .subscribe(data => {
                    this.cityname=data.user.city;
                });
        }).catch((error) => {
            this.loadingService.hideLoading();
        })
    }
    getnewData(infiniteScroll){
        ++this.pageNo;
            let storeId = this.navParams.get('storeId');
            this.globalProvider.getflyerfeed(this.locationdata.coords.latitude, this.locationdata.coords.longitude,/*28.6315, 77.2167,*/ storeId,this.pageNo)
                .subscribe(result => {
                    if(infiniteScroll){
                        infiniteScroll.complete();
                    }
                    for (let myval of result.flyerDtoList) {
                        this.cards.push(myval);
                        console.log(this.cards);
                    }
                    if(result.message== "NO EVENTS AT THIS MOMENT" && this.pageNo>0){
                        this.noapicall=false;
                        this.loadingService.showNotification("No More Events to show","right","");
                    }else {
                    //    this.metadata = result.flyerDtoList;
                   //     this.showdata=this.showdata.concat(result.flyerDtoList);
                    }
                });
    }

  uninterest(item,index) {
      console.log(item.id);
      this.cards.splice( index, 1 );
      // console.log(this.cards.length);
      if(this.cards.length==1) {
          this.getnewData("");
      }
        this.globalProvider.avoidbyid(item.id).subscribe(data => {
      });
  }

  interest(item,index) {
      this.cards.splice( index, 1 );
      console.log(this.cards.length);
      if(this.cards.length==1) {
          this.getnewData("");
      }
    this.globalProvider.getinterstbyid(item.id)
      .subscribe(data => {
          console.log(data);
     });
 //     this.checkCount(100,0,-30);
  }

  getinfo(id) {
    this.navCtrl.push('UserDetailPage', {
      userid: id
    });
  }
public exit(){
    let toast = this.toastCtrl.create({
        message: "Enable App Location and Relaunch APP",
        duration: 3000,
        position: 'top'
    });
    toast.onDidDismiss(() => {
        this.platform.exitApp();
    });
    toast.present();
}

    goToChat() {
    this.navCtrl.push('NewchatPage');
  }
  goToNotifications() {
    this.navCtrl.push('NotificationsPage');
  }

    // onThrowOut(event: ThrowEvent) {
    //     console.log('Hook from the template', event.throwDirection);
    // }
}
