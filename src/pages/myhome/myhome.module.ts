import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MyhomePage } from './myhome';
import { Diagnostic } from '@ionic-native/diagnostic';
import {LocationAccuracy} from "@ionic-native/location-accuracy";
import { OpenNativeSettings } from '@ionic-native/open-native-settings';
//import {SwingModule} from "angular2-swing";

@NgModule({
  declarations: [
    MyhomePage,

  ],
  imports: [
 //     SwingModule,
    IonicPageModule.forChild(MyhomePage),
  ],
    providers: [
        Diagnostic,LocationAccuracy, OpenNativeSettings
        ]
})
export class MyhomePageModule {}
