import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SearchTabPage } from './search-tab';
import {SuperTabsModule} from "ionic2-super-tabs";

@NgModule({
  declarations: [
    SearchTabPage,
  ],
  imports: [
    IonicPageModule.forChild(SearchTabPage),
      SuperTabsModule.forRoot(),
  ],
})
export class SearchTabPageModule {}
