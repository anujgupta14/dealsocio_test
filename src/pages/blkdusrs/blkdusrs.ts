import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { GlobalProvider } from '../../providers/global/global';
//import { UserDetailPage } from '../user-detail/user-detail';
/**

/**
 * Generated class for the BlkdusrsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-blkdusrs',
  templateUrl: 'blkdusrs.html',
})
export class BlkdusrsPage {
	public blockedlist : any=[];
  public baseURL = this.globalProvider.baseURL;
 constructor(public navCtrl: NavController,public globalProvider: GlobalProvider) {
  }

  ionViewDidLoad() {
    this.getlist();
  }
   getlist(){
		this.globalProvider.blockList()
        .subscribe(data => {
            this.blockedlist = data.contacts;
        });
    }
   unblock(id){
    this.globalProvider.unblockUserById(id)
        .subscribe(data => {
            this.ionViewDidLoad();
        });
    }
    getinfo(id){
          this.navCtrl.push('UserDetailPage', {
          userid : id
      });
    }
  getsana(url){
    return this.globalProvider.sanatizeurl(url);
  }
}
