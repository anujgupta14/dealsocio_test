import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BlkdusrsPage } from './blkdusrs';

@NgModule({
  declarations: [
    BlkdusrsPage,
  ],
  imports: [
    IonicPageModule.forChild(BlkdusrsPage),
  ],
})
export class BlkdusrsPageModule {}
