import {Component, ViewChild} from '@angular/core';
import { Camera, CameraOptions } from '@ionic-native/camera';
import {IonicPage, NavController, NavParams, FabContainer,Platform} from 'ionic-angular';
import { GlobalProvider } from '../../providers/global/global';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { Crop } from '@ionic-native/crop';
import {Storage} from "@ionic/storage";
import {LoadingProvider} from "../../providers/loading/loading";

@IonicPage()
@Component({
  selector: 'page-gallery',
  templateUrl: 'gallery.html',
})

export class GalleryPage {
    @ViewChild('fab') fab: FabContainer;
  public userId = this.navParams.get('userId');
    public baseURL = this.globalProvider.baseURL;
    public userinfo = this.globalProvider.userDetails;
    //public mydetail: any = {};
    public myprofilepic :any="";
    public i =0;
  public userImages:any;
  public base64Image:any;
  public mytime:any=new Date().getTime() +(++this.i);
    public profiledetails : any="";
  constructor(public platform: Platform,private transfer: FileTransfer,public loadingService:LoadingProvider,public storage: Storage,private crop: Crop,public navCtrl: NavController, public navParams: NavParams,
    public globalProvider: GlobalProvider, private camera: Camera) {
      if( localStorage.getItem('uploadCount')){

      }else
      localStorage.setItem('uploadCount', '0');
  }

  imageURI:any;
    ionViewWillEnter(){
        console.log("ionic will enter function");
        this.globalProvider.getmyprof()
            .subscribe(data => {
                this.profiledetails = data.user;
                this.userImages= data.user.images;
                console.log(this.userImages);
            this.myprofilepic=this.baseURL+this.profiledetails.profilePic+'?timestamp='+this.mytime+(++this.i);
            });
    }
  public getImage() {
       const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.FILE_URI,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      targetWidth: 1280,
      targetHeight: 1280
    }

    this.camera.getPicture(options).then((imageData) => {
       /* if (typeof this.fab != "undefined") {
            this.hideFab();
        } */
      this.imageURI = imageData;
      this.cropImage();
    }, (err) => {
      console.log(err);
    });
  }

public takePicture() {
  const options: CameraOptions = {
    quality: 100,
    destinationType: this.camera.DestinationType.FILE_URI,
    encodingType: this.camera.EncodingType.JPEG,
    mediaType: this.camera.MediaType.PICTURE,
    targetWidth: 1280,
    targetHeight: 1280
  }
  this.camera.getPicture(options).then((imageData) => {
  console.log(imageData);
     /* if (typeof this.fab != "undefined") {
          this.hideFab();
      } */
    this.imageURI = imageData;
     // this.uploadImage(this.imageURI);
      this.cropImage();
  }, (err) => {
    console.log(err);
    // Handle error
  });
}
public cropImage(){
    console.log('file://' + this.imageURI) ;
    if (this.platform.is('android')) {
        this.crop.crop('file://' + this.imageURI, {quality: 100})
            .then(
                newImage => this.uploadImage(newImage),
                error => console.error('Error cropping image', error)
            );
    }else if (this.platform.is('ios')) {
        this.crop.crop(this.imageURI, {quality: 100})
            .then(
                newImage => this.uploadImage(newImage),
                error => console.error('Error cropping image', error)
            );
    }
};

    hideFab(onlyKb=false){
        if(onlyKb){
            (<HTMLElement>document.querySelector("ion-footer")).setAttribute('style', 'bottom: 0');
        } else {
            (<HTMLElement>document.querySelector("ion-footer")).setAttribute('style', 'bottom: 0');
            if (typeof this.fab != "undefined") {
                this.fab.close();
            }
        }
    }
   public  showImage(imageUrl){
       this.navCtrl.push('ShowPicturePage', {
           pictureSrc : imageUrl,from :"Gallery"
       });
   }

public uploadImage (uriImage){
    const fileTransfer: FileTransferObject = this.transfer.create();
    let filename = uriImage.split('/').pop();
    let options1: FileUploadOptions = {
        chunkedMode: false,
        fileKey: "file",
        fileName: filename,
        mimeType: "multipart/form-data",
        headers: {
            "userid": localStorage.getItem('myuserUserId'),
            "token": localStorage.getItem('myuserToken'),
        },
    };
    console.log(options1);
    this.loadingService.showLoading("Image uploading");
    fileTransfer.upload(uriImage, this.globalProvider.serverURL + "/user/image/add", options1)
        .then((data) => {
            console.log(data);
            this.loadingService.hideLoading();
            var test= JSON.parse(data.response);
                    if(test.successful){
                        this.loadingService.popNotification(test.message,'success');
                    }else{
                        this.loadingService.popNotification("Something went wrong in image upload",'success');
                    }
        }, (err) => {
            this.loadingService.hideLoading();
            console.log(err);
            console.error(err);
        });
    //     this.loadingService.showLoading("Image uploading");
    //     this.globalProvider.uploadIMage(uriImage, options1).then(data => {
    //         this.loadingService.hideLoading();
    //         console.log('Token Register: ',data);
    //         console.log(data.response);
    //         console.log(data.response.successful);
    //         var test= JSON.parse(data.response);
    //         if(test.successful){
    //             let toast = this.toastCtrl.create({
    //                 message: test.message,
    //                 duration: 2000,
    //                 position: 'middle'
    //             });
    //             toast.onDidDismiss(() => {
    //                 console.log('Dismissed toast');
    //                 this.navCtrl.pop();
    //             });
    //             toast.present();
    //         }else{
    //             let toast = this.toastCtrl.create({
    //                 message: "Something went wrong in image upload",
    //                 duration: 2000,
    //                 position: 'middle'
    //             });
    //             toast.onDidDismiss(() => {
    //                 console.log('Dismissed toast');
    //                 this.navCtrl.pop();
    //             });
    //             toast.present();
    //         }
    //     }, (err) => {
    //         this.loadingService.hideLoading();
    //         console.error(err);
    //         console.log(err);  // <------ Or debug goes here (2)
    //     }).catch((err)=>{
    //         this.loadingService.hideLoading();
    //         console.error(err);
    //
    //     });
}
}

