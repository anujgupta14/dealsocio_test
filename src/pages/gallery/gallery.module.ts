import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GalleryPage } from './gallery';
import { Crop } from '@ionic-native/crop';
import { Camera } from '@ionic-native/camera';
//import { LazyLoadImageModule, intersectionObserverPreset } from 'ng-lazyload-image';

@NgModule({
  declarations: [
    GalleryPage,
  ],
  imports: [
    IonicPageModule.forChild(GalleryPage),
      // LazyLoadImageModule.forRoot({
      //     preset: intersectionObserverPreset
      // })
  ],
    providers: [
        Crop,
        Camera
  ]
})
export class GalleryPageModule {}
