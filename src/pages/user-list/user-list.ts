import { Component } from '@angular/core';
import {IonicPage, NavController, NavParams, AlertController, MenuController, Events} from 'ionic-angular';
import { GlobalProvider } from '../../providers/global/global';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the UserListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-user-list',
  templateUrl: 'user-list.html',
})
export class UserListPage {
  public acceptList : any=[];
  public Fid = this.navParams.get("flyerId");
  public baseURL = this.globalProvider.baseURL;
 constructor(public navCtrl: NavController,public events: Events, public navParams: NavParams,public globalProvider: GlobalProvider, public storage: Storage,public alertCtrl: AlertController, public menuCtrl: MenuController) {
  }

  ionViewDidLoad() {
    this.globalProvider.getlistacceptbyid(this.Fid)
        .subscribe(data => {
            this.acceptList = data.userDtoList;
        });
    }
    ionViewWillLeave(){
     console.log("inside will leave ");
     //   this.events.publish('user:event', "");
    }
    removeUser(uid){
      this.globalProvider.removeParticipant(this.Fid,uid)
        .subscribe(data => {
            this.ionViewDidLoad();
        });
    }
    getinfo(id){
          this.navCtrl.push('UserDetailPage', {
          userid : id
      });
    }
    goToChatWindow(chatUser){
        this.navCtrl.push('ChatWindPage', {
            userId : chatUser.id,
            userName : chatUser.name,
            userProfilePic : chatUser.profilePic
        });
    }
}
