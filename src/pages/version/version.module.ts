import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VersionPage } from './version';
import {InAppBrowser} from "@ionic-native/in-app-browser";

@NgModule({
  declarations: [
    VersionPage,
  ],
  imports: [
    IonicPageModule.forChild(VersionPage),
  ],
    providers: [InAppBrowser]
})
export class VersionPageModule {}
