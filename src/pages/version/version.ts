import { Component } from '@angular/core';
import {IonicPage, Platform} from 'ionic-angular';
import {InAppBrowser} from '@ionic-native/in-app-browser';
/**
 * Generated class for the VersionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-version',
  templateUrl: 'version.html',
})
export class VersionPage {

    constructor(public iab : InAppBrowser,public platform: Platform) {
    }

    closeModal(){
        if (this.platform.is('android')) {
            this.iab.create("https://play.google.com/store/apps/details?id=com.moglix.runner", "_system", "location=yes");
        }else if (this.platform.is('ios')){
            this.iab.create("https://apps.apple.com/in/app/venuelook-vlm-for-business/id1386822233", "_system", "location=yes")
        }
    }
    ngOnInit(){

    }

}
