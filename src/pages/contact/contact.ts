import { Component } from '@angular/core';
import { NavController, NavParams} from 'ionic-angular';
import { GlobalProvider } from '../../providers/global/global';
//import { UserDetailPage } from '../user-detail/user-detail';

@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html'
})
export class ContactPage {
	public contactlist : any;
	public userid = this.navParams.get('userid');
	public baseURL = this.globalProvider.baseURL;
	constructor(public navCtrl: NavController, public navParams: NavParams,public globalProvider: GlobalProvider) {
	}

	ionViewDidLoad() {
    	this.getlist(this.userid);
  	}
   	getlist(id){
		this.globalProvider.contactListById(id)
        .subscribe(data => {
            this.contactlist = data.contacts;
        });
    }
    removeById(id){
		this.globalProvider.removeContactById(id)
        .subscribe(data => {
        });
    }
    getinfo(id){
          this.navCtrl.push('UserDetailPage', {
          userid : id
      });
    }
}
