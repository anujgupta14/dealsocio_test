import {Component} from '@angular/core';
import {IonicPage, NavController,ToastController, NavParams, PopoverController} from 'ionic-angular';
import { GlobalProvider } from '../../providers/global/global';
import { Storage } from '@ionic/storage';

@IonicPage()
@Component({
  selector: 'user-detail',
  templateUrl: 'user-detail.html',
})
export class UserDetailPage {
    page1 = 'UserProfilePage';
    page2 = 'UserEventPage';
  public userinfo : any;
  public myIndexno : number=0;
  public myActiveSlide : number =0;
  public userFirstName : any;
  public userLastName : any;
  public userid = this.navParams.get('userid');
  public baseURL = this.globalProvider.baseURL;
  options = 'information';
  showAccord: Array<boolean> = [];
  userProfileDetailFlag = false;
  constructor(public popoverCtrl: PopoverController,private toastCtrl: ToastController,public navCtrl: NavController, public navParams: NavParams,public globalProvider: GlobalProvider, public storage: Storage) {
   // this.getuserbyid(this.userid);
  }


  ionViewDidLoad() {
    this.getuserbyid(this.userid);
  }

  getuserbyid(id){
//    id = '5a3e69af5fd6bf0c51e15dcf';
    this.globalProvider.getuserbyid(id)
        .subscribe(data => {
          this.userinfo = data.user;
          this.showAccord = Array(this.userinfo.flyers.length).fill(false);
          console.log(this.showAccord);
          this.userFirstName = data.user.firstName;
          this.userLastName = data.user.lastName;
          this.setvalue(data.user);
          this.storage.set('detailuser',data.user);
            this.userProfileDetailFlag = true;
        });
  }
  setvalue(info)
  {
        this.userinfo = info;
        console.log("infodetails",info);
  }

  reportFlyer(id,message,flyer){
    id = '5a3e69af5fd6bf0c51e15dcf';
    this.globalProvider.reportUserByIdFlyer(id,message,flyer)
        .subscribe(data => {
          console.log(data);
        });
  }
  connectionList(id){
    id = '5a3e69af5fd6bf0c51e15dcf';
    this.globalProvider.contactListById(id)
        .subscribe(data => {
          console.log(data);
        });
  }

  goToChatWindow(chat){
    this.navCtrl.push('ChatWindPage', {
      userId : this.userinfo.id,
      userName : this.userinfo.name,
      userProfilePic : this.userinfo.profilePic
    });
  }


    showImage(imageUrl){
        this.navCtrl.push('ShowPicturePage', {
            pictureSrc : imageUrl
        });
    }
    presentProfileModal() {
        let profileModal = this.popoverCtrl.create('PopoverPage',{UserInfo:this.userinfo});
        profileModal.present();
    }
    removeById(id){
        this.globalProvider.removeContactById(id)
            .subscribe(data => {
                if(data.successful){
                    this.userinfo.friend=false;
                    this.userinfo.friendRequestSent=false;
                    this.userinfo.friendRequestReceived=false;
                  this._successCallback("Removed");
                }else{
                    this._errorCallback(data.errors[0].description);
                }
            });
    }
    public doRequest(id,action){
        this.globalProvider.connectionRequestById(id,action)
            .subscribe(data => {
                if(data.successful){
                    if(action){
                        this.userinfo.friendRequestSent=true;
                        this._successCallback("Sent");
                    }else {
                        this.userinfo.friend=false;
                        this.userinfo.friendRequestSent=false;
                        this.userinfo.friendRequestReceived=false;
                        this._successCallback("Canceled");
                    }
                }else{
                    this._errorCallback(data.errors[0].description);
                }
            });
    }
    public doAction(id,action){
        this.globalProvider.actiononrequest(id,action)
            .subscribe(data => {
                if(data.successful){
                    if(action){
                        this.userinfo.friend=true;
                        this.userinfo.friendRequestSent=false;
                        this.userinfo.friendRequestReceived=false;
                        this._successCallback("Accepted");
                    }else {
                        this.userinfo.friend=false;
                        this.userinfo.friendRequestSent=false;
                        this.userinfo.friendRequestReceived=false;
                        this._successCallback("Rejected");
                    }
                }else{
                    this._errorCallback(data.errors[0].description);
                }
            });
    }
    public _successCallback(myMessage){
        let toast = this.toastCtrl.create({
            message: myMessage,
            duration: 1000,
            position: 'top'
        });
        toast.onDidDismiss(() => {
            console.log('Dismissed toast');
        });
        toast.present();
    }
    public _errorCallback(myError){
        let toast = this.toastCtrl.create({
            message: myError,
            duration: 2000,
            position: 'top'
        });
        toast.onDidDismiss(() => {
            console.log('Dismissed toast');
        });
        toast.present();
    }
    friendlist(){
        this.navCtrl.push('OthersfriendPage',{'otherUser':this.userid});
    }
     goToGoogleMap(mapData){
            window.open('geo:0,0?q='+mapData.coordinates[1]+','+mapData.coordinates[0], '_system');
        }
}
