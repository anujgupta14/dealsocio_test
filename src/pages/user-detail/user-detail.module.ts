import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UserDetailPage } from './user-detail';
import {SuperTabsModule} from "ionic2-super-tabs";

@NgModule({
  declarations: [
    UserDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(UserDetailPage),
      SuperTabsModule.forRoot(),
  ],
})
export class UserDetailPageModule {}
