import { Component } from '@angular/core';
import { IonicPage } from 'ionic-angular';
import { Platform, ViewController } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { Observable, Subscription } from 'rxjs';

const DEFAULT_ZOOM = 8;
const EQUATOR = 40075004;
const DEFAULT_LAT = 28.7041;
const DEFAULT_LNG = 77.1025;

@IonicPage()
@Component({
    selector: 'page-locationshare',
    templateUrl: 'locationshare.html',
})
export class LocationsharePage {

    lat: number = DEFAULT_LAT;
    lng: number = DEFAULT_LNG;
    zoom: number = DEFAULT_ZOOM;
    accuracy: number = -1;
    intervalObs: Subscription;

    constructor(private platform: Platform,
                private viewCtrl: ViewController,
                private geolocation: Geolocation) {
    }

    ionViewDidEnter(){
        return Observable.fromPromise(this.geolocation.getCurrentPosition().then((position) => {
            // Update view-models to represent the current geo-location
            this.accuracy = position.coords.accuracy;
            this.lat = position.coords.latitude;
            this.lng = position.coords.longitude;
        }));
    }

    ngOnDestroy() {
        // Dispose subscription
        if (this.intervalObs) {
            this.intervalObs.unsubscribe();
        }
    }

    calculateZoomByAccureacy(accuracy: number): number {
        // Source: http://stackoverflow.com/a/25143326
        const deviceHeight = this.platform.height();
        const deviceWidth = this.platform.width();
        const screenSize = Math.min(deviceWidth, deviceHeight);
        const requiredMpp = accuracy / screenSize;

        return ((Math.log(EQUATOR / (256 * requiredMpp))) / Math.log(2)) + 1;
    }

    reloadLocation() {
        console.log('in reloadlocation');
        return Observable.fromPromise(this.geolocation.getCurrentPosition().then((position) => {
            if (this.lat && this.lng) {
                // Update view-models to represent the current geo-location
                this.accuracy = position.coords.accuracy;
                this.lat = position.coords.latitude;
                this.lng = position.coords.longitude;
                this.zoom = this.calculateZoomByAccureacy(this.accuracy);

                this.sendLocation();
            }
        }));
    }

    dragEnded($event){
        console.log($event);
        this.lat = $event.coords.lat;
        this.lng = $event.coords.lng;
    }

    sendLocation() {
        this.viewCtrl.dismiss({
            lat: this.lat,
            lng: this.lng,
            zoom: this.zoom
        });
    }
}
