import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LocationsharePage } from './locationshare';
import {AgmCoreModule} from "@agm/core";

@NgModule({
    declarations: [
        LocationsharePage,
    ],
    imports: [
        IonicPageModule.forChild(LocationsharePage),
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyA0AAM--_7PA_QT4KNzqCpq70BNgqN16wU'
        }),
    ],
})
export class LocationsharePageModule {}
