import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GlobalProvider } from '../../providers/global/global';
import {LoadingProvider} from "../../providers/loading/loading";

/**
 * Generated class for the NotificationsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-notifications',
  templateUrl: 'notifications.html',
})
export class NotificationsPage {

	public notifications: any;
  pageNumber:number = 0;
  infiniteEnabled: boolean = true;

  constructor(public navCtrl: NavController,public loadingService:LoadingProvider, public navParams: NavParams,public globalProvider: GlobalProvider) {

  }
  ionViewDidEnter() {
      this.loadingService.showLoading("Notification List");
    //this.globalProvider.unreadNotifications = 0;
    this.pageNumber = 0;
    this.globalProvider.fetchAllNotifications({pageInfo: {pageNumber: this.pageNumber, pageSize: 50, sortOrder: "desc", sortColumn: "createdOn"}})
      .subscribe(data => {
        this.notifications = data.notificationList;
          this.loadingService.hideLoading();
    });
    this.globalProvider.ackNotifications()
      .subscribe(data => {
          this.loadingService.hideLoading();
    });
  }

  goToNotificationRedirect(notification){
  	if(notification.action == 'profile'){
      	this.navCtrl.push('UserDetailPage', {
          userid : notification.userData.userId
        });
    } else if(notification.action == 'blocked'){

    } else if(notification.action == 'flyer_request_list'){
        this.navCtrl.push('FlyerreqPage', {
          flyerId : notification.flyerData.flyerId
        });
    } else if(notification.action == 'message'){
    	this.navCtrl.push('ChatWindowPage', {
        	userId : notification.userId,
	          userName : "",
	          userProfilePic : notification.image
        });
    } else {
      //this.navCtrl.push(HomePage);
    }
  }

}
