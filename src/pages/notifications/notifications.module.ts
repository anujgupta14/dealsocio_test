import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NotificationsPage } from './notifications';
//import {NiceDateFormatPipe} from "../../providers/global/niceDateFormatPipe";
import {PipesModule} from "../../pipes/pipes.module";

@NgModule({
  declarations: [
    NotificationsPage,

  ],
  imports: [
    IonicPageModule.forChild(NotificationsPage),
      PipesModule
  ],
})
export class NotificationsPageModule {}
