import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NewchatPage } from './newchat';
import {PipesModule} from "../../pipes/pipes.module";

@NgModule({
  declarations: [
    NewchatPage,
  ],
  imports: [
    IonicPageModule.forChild(NewchatPage),
      PipesModule,
  ],
})
export class NewchatPageModule {}
