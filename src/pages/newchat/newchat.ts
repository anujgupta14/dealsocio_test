import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, Navbar, ActionSheetController } from 'ionic-angular';
import { GlobalProvider } from '../../providers/global/global';
import { Observable } from 'rxjs/Observable';
import {Message} from '@stomp/stompjs';
import { Subscription } from 'rxjs/Subscription';
import {StompRService} from '@stomp/ng2-stompjs';
/**
 * Generated class for the NewchatPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-newchat',
  templateUrl: 'newchat.html',
})
export class NewchatPage {
    @ViewChild(Navbar) navBar: Navbar;
    public chats : any;
    public baseURL = this.globalProvider.baseURL;
    showSearchBar: boolean = false;
    search_text: string = "";

    // Stream of messages
    private subscription: Subscription;
    public messages: Observable<Message>;

    // Subscription status
    public subscribed: boolean;

    // A count of messages received
    public count = 0;

    pageNumber:number = 1;
    infiniteEnabled: boolean = true;
    constructor(public navCtrl: NavController, public globalProvider: GlobalProvider, public actionSheetCtrl: ActionSheetController, private _stompService: StompRService) {

    }

    ionViewDidLoad() {

        this.subscribed = false;
        // Store local reference to Observable
        // for use with template ( | async )
        this.subscribe();

        this.navBar.backButtonClick = (e:UIEvent)=>{
            this.navCtrl.pop();
        }
    }

    ionViewDidEnter() {
        console.log('ionViewDidLoad Chat');
        this.getConversations();
        this.showSearchBar = false;
    }

    public subscribe() {
        if (this.subscribed) {
            return;
        }

        // Stream of messages
        this.messages = this._stompService.subscribe('/user/dealsocio/getChat');

        console.log('in subscribe');

        // Subscribe a function to be run on_next message
        this.subscription = this.messages.subscribe(this.on_next, (error)=>{
            console.log('error in subscribing');
        });

        this.subscribed = true;
    }

    /** Consume a message from the _stompService */
    public on_next = (message: Message) => {

        console.log('in on_next');

        // Store message in "historic messages" queue

        let msgBody = JSON.parse(message.body);
        let existingChat = false;

        for (var _i = 0; _i < this.chats.length; _i++) {
            if(this.chats[_i].userId == msgBody.senderId){

                this.chats[_i].lastMessageTime = msgBody.createdOn;
                this.chats[_i].lastMessage = msgBody.message;
                this.chats[_i].read = false;

                if(msgBody.hasOwnProperty('dataType')){
                    this.chats[_i].lastDataType = msgBody.dataType;
                } else {
                    this.chats[_i].lastDataType = null;
                }

                existingChat = true;
                break;
            }
        }

        if(!existingChat){

            let lastDataType = null;
            if(msgBody.hasOwnProperty('dataType')){
                lastDataType = msgBody.dataType;
            }

            this.chats.push({
                userId: msgBody.senderId,
                userName: msgBody.senderName,
                userProfilePic: msgBody.senderImage,
                lastMessageTime: msgBody.createdOn,
                read: false,
                lastDataType: lastDataType,
                lastMessage: msgBody.message
            });
        }

        // Count it
        this.count++;
    }



    getConversations(){
        this.globalProvider.getConversations(this.globalProvider.userDetails.session.userId)
            .subscribe(data => {
                console.log(data);
                this.chats = data.conversations;
            });
    }

    /*presentActionSheet() {
	   let actionSheet = this.actionSheetCtrl.create({
	     title: ''
	   });
	   actionSheet.addButton({ text: 'Option 1',handler: () => this.getConversations()});
	   actionSheet.addButton({ text: 'Option 2',handler: () => this.getConversations()});
	   actionSheet.present();
    }*/

    goToChatWindow(chat){
        // make current conversation read
        for (var _i = 0; _i < this.chats.length; _i++) {
            if(this.chats[_i].userId == chat.userId){
                this.chats[_i].read = true;
                break;
            }
        }

        this.navCtrl.push('ChatWindPage', {
            userId : chat.userId,
            userName : chat.userName,
            userProfilePic : chat.userProfilePic
        });
    }

    goToNewChatUserList(){
        this.navCtrl.push('NewchatlistPage');
    }

    invertSearchBar(){
        this.showSearchBar = !this.showSearchBar;
    }

    setScrollContentStyle(){
        let styles = {
            'margin-top': this.showSearchBar ? '50px' : ''
        };
        return styles;
    }

    pressEventChat(chat){
        let actionSheetChat = this.actionSheetCtrl.create({
            title: ''
        });
        actionSheetChat.addButton({ text: 'Contact Info',handler: () => this.goToProfile(chat.userId)});
        actionSheetChat.addButton({ text: 'Delete Chat',handler: () => this.deleteConversation(chat)});
        actionSheetChat.present();
    }

    goToProfile(userId){
        this.navCtrl.push('UserDetailPage', {
            userid : userId
        });
    }

    deleteConversation(chat){
        this.globalProvider.deleteConversation({counterUserId: chat.userId})
            .subscribe(data => {
                if(data.successful){
                    let index = this.chats.indexOf(chat);
                    if(index > -1){
                        this.chats.splice(index, 1);
                    }

                }
            });
    }


}
