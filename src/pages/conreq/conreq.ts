import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { GlobalProvider } from '../../providers/global/global';
//import { UserDetailPage } from '../user-detail/user-detail';
/**
/**
 * Generated class for the ConreqPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-conreq',
  templateUrl: 'conreq.html',
})
export class ConreqPage {
	public reqlist : any=[];
    public baseURL = this.globalProvider.baseURL;

    constructor(public navCtrl: NavController, public globalProvider: GlobalProvider) {
  }

    ionViewWillEnter() {
        this.getlist();
  }
   getlist(){
		this.globalProvider.contactRequestListById()
        .subscribe(data => {
            this.reqlist = data.contacts;
        });
    }
    actionReq(id,status){
      this.globalProvider.actiononrequest(id,status)
        .subscribe(data => {
            this.getlist();
        });
    }
    getinfo(id){
          this.navCtrl.push('UserDetailPage', {
          userid : id
      });
    }
}
