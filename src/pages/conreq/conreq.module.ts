import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ConreqPage } from './conreq';

@NgModule({
  declarations: [
    ConreqPage,
  ],
  imports: [
    IonicPageModule.forChild(ConreqPage),
  ],
})
export class ConreqPageModule {}
