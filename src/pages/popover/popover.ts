import { Component } from '@angular/core';
import {AlertController, App, IonicPage, NavController, NavParams,ViewController} from 'ionic-angular';
import {GlobalProvider} from "../../providers/global/global";

/**
 * Generated class for the PopoverPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-popover',
  templateUrl: 'popover.html',
})
export class PopoverPage {
    public userinfo : any;
  constructor(public app: App,public viewCtrl: ViewController,public globalProvider: GlobalProvider,public alertCtrl: AlertController,public navCtrl: NavController, public navParams: NavParams) {
      console.log(this.navParams.data.UserInfo);
      this.userinfo=this.navParams.data.UserInfo
      console.log('ionViewDidLoad PopoverPage');
  }
    public sendRequest(id){
        console.log('Request clicked');
        this.globalProvider.connectionRequestById(id,true)
            .subscribe(data => {
                console.log(data);
                this.viewCtrl.dismiss();
            });
    }
    public cancelRequest(id){
        this.globalProvider.connectionRequestById(id,false)
            .subscribe(data => {
                console.log(data);
                this.viewCtrl.dismiss();
            });
    }
    public blockuser(id){
        this.globalProvider.blockUserById(id)
            .subscribe(data => {
                console.log(data);
                this.viewCtrl.dismiss();
            });
    }
    public report(id,message){
        //this.showAlert(id,message);
       // this.viewCtrl.dismiss();
        this.viewCtrl.dismiss();
       // this.app.getRootNav().push('ReasonPage',{'id':id});
        this.navCtrl.push('ReasonPage',{'id':id})
    }
    public  showAlert(id,message){
        console.log(id);
        console.log(message);
        // let alert = this.alertCtrl.create({
        //     title: 'Take Action',
        //     inputs: [
        //         {
        //             name: 'reason',
        //             placeholder: 'Report Here'
        //         }
        //     ],
        //     buttons: [
        //         {
        //             text: 'Cancel',
        //             role: 'cancel',
        //             handler: data => {
        //                 console.log('Cancel clicked');
        //             }
        //         },
        //         {
        //             text: 'Report',
        //             cssClass : 'ButtonCss',
        //             handler: data => {
        //                 console.log(data.reason);
        //                 if(data.reason){
        //                     console.log("fine");
        //                     this.globalProvider.reportUserById(id,data.reason)
        //                         .subscribe(data => {
        //                             console.log(data);
        //                         });
        //                 }else{
        //                     console.log("please type some Reason");
        //                     let toast = this.toastCtrl.create({
        //                         message: 'Please type some Reason',
        //                         duration: 3000,
        //                         position: 'top'
        //                     });
        //                     toast.onDidDismiss(() => {
        //                         console.log('Dismissed toast');
        //                     });
        //                     toast.present();
        //                     return false;
        //                 }
        //
        //             }
        //         }
        //     ]
        // });
    }
}
