import {Component, ViewChild} from '@angular/core';
import {IonicPage, NavController, NavParams, Slides} from 'ionic-angular';
import { GlobalProvider } from '../../providers/global/global';
import 'rxjs/Rx';
import { Storage } from '@ionic/storage';
/**
 * Generated class for the SearchPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-search',
  templateUrl: 'search.html',
 providers: [GlobalProvider],
 })
export class SearchPage {
    @ViewChild(Slides) slides: Slides;
  public userdata: any;
  public recentUser:any;
  public recentPlace:any=[];
  public myActiveSlide : number =0;
  public searchTerm: string = '';
  public userTerm: string = '';
  public storedata: any;
  public events: any;
  public baseURL = this.globalProvider.baseURL;
  public unreadCount: number = 0;
    public unreadNotify: number = 0;
  constructor(public navCtrl: NavController,public storage: Storage, public globalProvider: GlobalProvider,
              public navParams: NavParams) {
    this.events = "users";
    this.userdata = [];
    this.recentUser=[];
    this.storedata = [];

  }
    ionViewWillEnter(){
        this.storage.get('userDetails').then((data) => {
            this.globalProvider.getRecentSearch()
                .subscribe(data => {
                    this.recentUser=data.users;
                    console.log("** searched data users **");
                    console.log(this.recentUser);
                });
            this.globalProvider.getPlaceSearch()
                .subscribe(data => {
                    this.recentPlace=data.stores;
                });
        });
    }
    ionViewDidLoad() {
        const segBtn = document.getElementById('segmentSearch1');
        segBtn.classList.add('segment-activated');
    }
  getItems(ev) {
    var val = ev.target.value;
      if(val==undefined){
          this.cancelItem("");
      }else {
          if (val.length >= 1) {
              this.searchAllUsers(val);
          }
          else {
              this.userdata = [];
          }
      }
  }
    cancelPlace(evn){
        this.searchTerm='';
        this.storedata = [];
    }
    cancelItem(evn){
        this.userTerm='';
        this.userdata = [];
    }
    ionViewDidEnter(){
        this.globalProvider.getUnreadConversations()
            .subscribe(data => {
                this.unreadCount = data.count;
            });
        this.globalProvider.getUnreadNotification()
            .subscribe(data => {
                if(data.successful){
                    this.unreadNotify=data.count;
                }
            });
    }
    addinRecent(myType,clickedId){
      this.globalProvider.addRecentSearch(myType,clickedId)
            .subscribe(data => {
                console.log("get recent serach");
                console.log(data);
            });
    }
  searchAllUsers(res) {
    this.globalProvider.searchProduct(res, 'user').debounceTime(2000).distinctUntilChanged()
      .subscribe(data => {
        this.userdata = data.userList;
      });
  }

  getStores(ev) {
    var val = ev.target.value;
    this.searchTerm=ev.target.value;
    if(val==undefined){
      this.cancelPlace("");
    }else{
        if (val.length >= 1) {
            this.searchAllStores(val);
        }
        else{
            this.storedata = [];
        }
    }
  }

  searchAllStores(res) {
    this.globalProvider.searchProduct(res, 'store').debounceTime(2000).distinctUntilChanged()
      .subscribe(data => {
        this.storedata = data;
      });
  }

  getUserProfileInfo(id){
    this.navCtrl.push('UserDetailPage', {
      userid : id
    });
  }

  getPlaceInfo(exp){
      console.log(exp);
    this.navCtrl.push('PlaceDetailPage', {
      "id" : exp
    });
  }
    goToChat() {
        this.navCtrl.push('NewchatPage');
    }
    goToNotifications() {
        this.navCtrl.push('NotificationsPage');
    }
    public clearSearch(typeSearch){
        this.globalProvider.clearSearch(typeSearch)
            .subscribe(data => {
                if(data.successful){
                    if(typeSearch=="STORE"){
                        this.recentPlace=[];
                    }else{
                        this.recentUser=[];
                    }
                }

            });
    }
    goToSlide(slideIndex) {
        this.slides.slideTo(slideIndex, 500);
    }

    goTonextSlide(){
        if(this.slides.getActiveIndex()==0 || this.slides.getActiveIndex()==1){
            this.slides.lockSwipeToNext(false);
            this.myActiveSlide=this.slides.getActiveIndex();
        }else {
            this.slides.lockSwipeToNext(true);
        }
    }
}
