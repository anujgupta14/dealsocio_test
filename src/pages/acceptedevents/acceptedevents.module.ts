import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AcceptedeventsPage } from './acceptedevents';

@NgModule({
  declarations: [
    AcceptedeventsPage,
  ],
  imports: [
    IonicPageModule.forChild(AcceptedeventsPage),
  ],
})
export class AcceptedeventsPageModule {}
