import {
	Component,
	trigger,
	state,
	style,
	transition,
	animate
} from "@angular/core";
import {
	AlertController,
	IonicPage,
	NavController,
	NavParams
} from "ionic-angular";
import { GlobalProvider} from "../../providers/global/global";
import { LoadingProvider} from "../../providers/loading/loading";

/**
 * Generated class for the AcceptedeventsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
	selector: "page-acceptedevents",
	templateUrl: "acceptedevents.html",
	animations: [
		trigger("listItemState", [
			state(
				"in",
				style({
					opacity: 1,
					height: "*",
					minHeight: "*"
				})
			),
			transition("* => void", [
				animate(
					"0.25s",
					style({
						opacity: 0,
						height: "1px",
						minHeight: "1px",
						transform: "perspective(500px) translateZ(-400px) scale(1.2)"
					})
				)
			])
		])
	]
})
export class AcceptedeventsPage {
	public acc_events: any = [];
	public baseURL = this.globalProvider.baseURL;
	showAccord1: Array<boolean> = [];
	showAccord2: Array<boolean> = [];
	rootNavCtrl: NavController;
	constructor(
		private alertCtrl: AlertController,
		public globalProvider: GlobalProvider,
		public loadingService: LoadingProvider,
		public navCtrl: NavController,
		public navParams: NavParams
	) {
		this.rootNavCtrl = navParams.get("rootNavCtrl");
	}

	ionViewDidEnter() {
		this.getAcceptedList("");
	}
    getinfo(id) {
        this.globalProvider.makebackActive = true;
        this.rootNavCtrl.push('UserDetailPage', {
            userid: id
        });
    }
	getAcceptedList(info) {
		this.globalProvider.getlistaccepted().subscribe(data => {
			if (info) {
				info.complete();
			}
			this.acc_events = data.flyerDtoList;
			console.log(this.acc_events);
			this.showAccord1 = Array(100).fill(false);
			this.showAccord2 = Array(100).fill(false);
		});
	}
	goToChatWindow(chatUser) {
		this.globalProvider.makebackActive = true;
		console.log(this.globalProvider.makebackActive);
		this.rootNavCtrl.push("ChatWindPage", {
			userId: chatUser.userId,
			userName: chatUser.flyerName,
			userProfilePic: chatUser.flyerImage
		});
	}
	presentConfirm(id, index) {
		let alert = this.alertCtrl.create({
			// title: 'Leave',
			message: "Are You Sure?",
			buttons: [
				{
					text: "No",
					role: "cancel",
					handler: () => {
						console.log("Cancel clicked");
					}
				},
				{
					text: "Yes",
					handler: () => {
						this.removemyself(id, index);
					}
				}
			]
		});
		alert.present();
	}
	removemyself(flyerid, index) {
		this.acc_events.splice(index, 1);
		this.globalProvider.removeSelfFromAcceptedFlyer(flyerid).subscribe(data => {
			console.log(data);
		});
	}
	goToGoogleMap(mapData) {
		window.open(
			"geo:0,0?q=" + mapData.coordinates[1] + "," + mapData.coordinates[0],
			"_system"
		);
	}
}
