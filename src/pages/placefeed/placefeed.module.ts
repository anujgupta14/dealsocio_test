import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PlacefeedPage } from './placefeed';

@NgModule({
  declarations: [
    PlacefeedPage,
  ],
  imports: [
    IonicPageModule.forChild(PlacefeedPage),
  ],
})
export class PlacefeedPageModule {}
