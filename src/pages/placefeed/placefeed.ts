import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams} from 'ionic-angular';
import { GlobalProvider } from '../../providers/global/global';
import {LoadingProvider} from "../../providers/loading/loading";

@IonicPage()
@Component({
  selector: 'page-placefeed',
  templateUrl: 'placefeed.html',
})
export class PlacefeedPage {
  public userinfo = this.globalProvider.userDetails;
  public baseURL = this.globalProvider.baseURL;
  public locationdata: any;
  public metadata: any =[];
    public pageNo:number=0;
  public exp = this.navParams.get('experiance');
  public tagIconUrls = [];
public nodata:boolean= false;
    public noapiCall:boolean= true;
  constructor(public navCtrl: NavController,public loadingService:LoadingProvider, public navParams: NavParams, public globalProvider: GlobalProvider) {
    this.metadata = [];
    console.log(localStorage.getItem('myLatitude'));
      console.log(localStorage.getItem('myLongitude'));
  }

  ionViewWillEnter() {
    this.getData("");
  }
  getData(infiniteScroll){
      this.noapiCall=true;
      this.loadingService.showLoading("Place Feed");
      this.globalProvider.storefeed(this.exp.id, localStorage.getItem('myLatitude'), localStorage.getItem('myLongitude'),this.pageNo)
          .subscribe(data => {
              this.loadingService.hideLoading();
              ++this.pageNo;
              if(infiniteScroll){
                  infiniteScroll.complete();
              }
              console.log(data.stores.length);
              //this.metadata = data.stores;
              console.log(this.metadata);
              if(data.stores.length>0){
                  this.noapiCall=true;
                  for(let i = 0; i < data.stores.length; i++){
                      this.metadata.push(data.stores[i]);
                      console.log(data.stores[i]);
                      this.tagIconUrls[i] = [];
                      for(let tag of data.stores[i].tags){
                          let url = "assets/icons/" + tag + ".png";
                          this.tagIconUrls[i].push(url);
                      }
                  }
              }else{
                  this.noapiCall=false;
              }
              if(this.metadata.length==0){
                  this.nodata=true;
              }else{
                  this.nodata=false;
              }
          });
  }
  createplan(store = " ") {
      console.log(store);
      console.log(this.navParams.data);
    this.navCtrl.push('CreatePlanPage', {
      store: store, experianceName: this.navParams.data.experiance.name
    });
  }

  getsana(url) {
    return this.globalProvider.sanatizeurl(url);
  }
    feedList(index){
      console.log(this.metadata[index].id)
        this.navCtrl.push('MyhomePage', {
            storeId: this.metadata[index].id
        });
    }
}
