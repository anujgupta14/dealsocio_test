import { Component } from '@angular/core';
import {AlertController, IonicPage, NavController} from 'ionic-angular';
import { GlobalProvider } from '../../providers/global/global';
import {LoadingProvider} from "../../providers/loading/loading";

/**
/**
 * Generated class for the ProfileInfoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profile-info',
  templateUrl: 'profile-info.html',
})
export class ProfileInfoPage {
	public profiledetails : any = {};
  public phoneverified : any;
  public mailverified : any;
  constructor(public navCtrl: NavController,public alertCtrl: AlertController,public loadingService:LoadingProvider, public globalProvider: GlobalProvider) {

  }

  ionViewWillEnter() {
      this.loadingService.showLoading("Profile");
     this.globalProvider.getmyprof().subscribe(data => {
        this.profiledetails = data.user;
        console.log(this.profiledetails);
         this.loadingService.hideLoading();
     });
    if(this.profiledetails.phoneVerified)
     this.phoneverified="checkmark-circle";
    else
     this.phoneverified="checkmark";
  }
  navedit(){
        this.navCtrl.push('ProfileeditPage', {
          details : this.profiledetails
        });
  }
  phoneverify(){
      if(this.profiledetails.phoneVerified){
          this.loadingService.showNotification("phone is already verified", "right", "");
      }else{
          this.navCtrl.push('OtpgetPage');
      }
  }

    showAlert() {
        let alert = this.alertCtrl.create({
            title: 'Verification failed!',
            subTitle: 'Please try with correct details.',
            buttons: ['Try Again']
        });
        alert.present();
    }
    emailverify(){
        if(this.profiledetails.emailVerified){
            this.loadingService.showNotification("Email is already verified", "right", "");
        }else{
            this.navCtrl.push('VerifyemailPage',{'myEmail':this.profiledetails.email});
        }
    }
}
