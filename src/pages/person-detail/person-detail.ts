import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';


/**
 * Generated class for the PersonDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-person-detail',
  templateUrl: 'person-detail.html',
})
export class PersonDetailPage {
	public CCCometChat : any;

  constructor(public navCtrl: NavController, public navParams: NavParams,public plt: Platform) {
  	if (this.plt.is('android')) {
  		this.CCCometChat.getInstance(console.log("Success"),console.log("Failed"));
  	}
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PersonDetailPage');
  }

}
