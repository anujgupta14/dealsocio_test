import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,AlertController,MenuController} from 'ionic-angular';
import { Validators, FormBuilder} from '@angular/forms';
import { GlobalProvider } from '../../providers/global/global';
import { Storage } from '@ionic/storage';
import {LoadingProvider} from "../../providers/loading/loading";


/**
 * Generated class for the OtpgetPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-otpget',
  templateUrl: 'otpget.html',
})
export class OtpgetPage {
  public formData : any;
  public userinfo : any;
  constructor(public navCtrl: NavController,public loadingService:LoadingProvider, public navParams: NavParams, public formBuilder: FormBuilder,public globalProvider: GlobalProvider, public storage: Storage,public alertCtrl: AlertController, public menuCtrl: MenuController) {
  	  this.formData = this.formBuilder.group({
        phone: ['', Validators.required]
      });
      this.storage.get('userDetails').then((data) => {
        console.log(data);
          this.userinfo = data;
      });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OtpgetPage');
  }

  showAlert() {
      let alert = this.alertCtrl.create({
      title: 'Verification failed!',
      subTitle: 'Please try with correct details.',
      buttons: ['Try Again']
    });
      alert.present();
  }
  skip(){
  		this.navCtrl.push('ProfileInfoPage');
  }
  submitOtpForm() {
    var phone = this.formData.value.phone;
      this.loadingService.showLoading("Otp Submit");
    this.globalProvider.getphoneotp(phone).subscribe(data => {
    	console.log(data);
        if(data.successful){
            this.loadingService.hideLoading();
            this.navCtrl.push('OtpverPage');
        }
        else{
            this.loadingService.hideLoading();
          this.showAlert();
         }
    	});
	}
}
