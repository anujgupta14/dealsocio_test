import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OtpgetPage } from './otpget';

@NgModule({
  declarations: [
    OtpgetPage,
  ],
  imports: [
    IonicPageModule.forChild(OtpgetPage),
  ],
})
export class OtpgetPageModule {}
