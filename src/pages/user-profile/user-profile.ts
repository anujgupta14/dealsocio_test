import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {GlobalProvider} from "../../providers/global/global";
import {Storage} from "@ionic/storage";

/**
 * Generated class for the UserProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-user-profile',
  templateUrl: 'user-profile.html',
})
export class UserProfilePage {
    public userinfo : any={};
    public userid = this.navParams.get('userid');
    public userFirstName : any;
    public userLastName : any;
    showAccord: Array<boolean> = [];
    public baseURL = this.globalProvider.baseURL;
    userProfileDetailFlag = false;
    rootNavCtrl: NavController;
  constructor(public navCtrl: NavController, public storage: Storage,public globalProvider: GlobalProvider, public navParams: NavParams) {
      this.rootNavCtrl = navParams.get('rootNavCtrl');
      console.log("inside user profile");
      //this.getuserbyid(this.userid);
      this.userinfo=this.navParams.get('uuid');
      console.log(this.userinfo);
      this.userFirstName = this.userinfo.firstName;
      this.userLastName = this.userinfo.lastName;
  }
    showImage(imageUrl){
        this.rootNavCtrl.push('ShowPicturePage', {
            pictureSrc : imageUrl
        });
    }
}
