import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OthersfriendPage } from './othersfriend';

@NgModule({
  declarations: [
    OthersfriendPage,
  ],
  imports: [
    IonicPageModule.forChild(OthersfriendPage),
  ],
})
export class OthersfriendPageModule {}
