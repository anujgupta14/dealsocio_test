import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {GlobalProvider} from "../../providers/global/global";
import {LoadingProvider} from "../../providers/loading/loading";

/**
 * Generated class for the OthersfriendPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-othersfriend',
  templateUrl: 'othersfriend.html',
})
export class OthersfriendPage {
    public connections : any=[];
    public baseURL = this.globalProvider.baseURL;
  constructor(public loadingService:LoadingProvider,public globalProvider: GlobalProvider,public navCtrl: NavController, public navParams: NavParams) {
  console.log(this.navParams.get('otherUser'));
  }

  ionViewDidLoad() {
      this.getlist("");
    console.log('ionViewDidLoad OthersfriendPage');
  }
    getlist(refresh){
        this.loadingService.showLoading("Getting List");
        this.globalProvider.othersFrdListById(this.navParams.get('otherUser'))
            .subscribe(data => {
                if(refresh){
                    refresh.complete();
                }
                this.loadingService.hideLoading();
                if(data.successful){
                    this.connections = data.elements;
                    console.log(data.elements);
                }else {
                    this.loadingService.showNotification(data.errors,"wrong","");
                }

            });
    }
    userDetailPage(id){
        this.navCtrl.push('UserDetailPage', {
            userid : id
        });
    }
}
