import { Component,ViewChild } from '@angular/core';
import { GlobalProvider } from '../../providers/global/global';
import { Tabs ,Platform,NavController} from 'ionic-angular';
//import {Firebase} from "@ionic-native/firebase";
import {FCM} from "@ionic-native/fcm";
@Component({
  selector:'page-tabs',
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = 'MyhomePage';
  tab2Root = 'SearchTabPage';
  tab3Root = 'ExperiancePage';
  tab4Root = 'ProfilePage';
  tab5Root = 'NewEventPage';
  isVisible: boolean = true;
    isAndroid: boolean = false;
  @ViewChild('myTabs') tabRef: Tabs;
    //firebaseToken: any;
  constructor(private fcm: FCM,public platform: Platform, public navCtrl: NavController,public globalProvider: GlobalProvider) {
      this.isAndroid = platform.is('android');
      platform.ready().then(() => {
          this.fcm.getToken()
              .then(token => {
                     // this.firebaseToken = token;
                      console.log('TOKEN GET______ ',token);

                      this.globalProvider.fcmRegister(token)
                          .subscribe(data => {
                              console.log('Token Register: ',data);
                          });
                  }
              )
              .catch(error => console.error('Error getting token', error));

          this.fcm.onTokenRefresh()
              .subscribe((token: string) => {
                      //this.firebaseToken = token;
                      console.log('TOKEN REFRESH______ ',token);

                      this.globalProvider.fcmRegister(token)
                          .subscribe(data => {
                              console.log('Token Register: ',data);
                          });
                  }
              );
      });
  }
 ionViewDidEnter() {
 //this.tabRef.select(2);
  console.log(this.tabRef.getSelected());
/* this.zone.run(() => {
     this.navCtrl.setRoot(TabsPage);
 });
 */

  }
}
