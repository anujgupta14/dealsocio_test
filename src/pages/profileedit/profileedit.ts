import { Component } from "@angular/core";
import {
	IonicPage,
	NavController,
	NavParams,
	AlertController,
	MenuController
} from "ionic-angular";
import { Validators, FormBuilder } from "@angular/forms";
import { Storage } from "@ionic/storage";
import { GlobalProvider } from "../../providers/global/global";
import { LoadingProvider } from "../../providers/loading/loading";

/**
 * Generated class for the ProfileeditPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
	selector: "page-profileedit",
	templateUrl: "profileedit.html"
})
export class ProfileeditPage {
	public formData: any;
	public userName: any;
	public gender: any;
	public description: any;
	public myDate: any;
	public range: any;
	public occupation: any;
	public profiledetails: any;
	constructor(
		public navCtrl: NavController,
		public navParams: NavParams,
		public formBuilder: FormBuilder,
		public storage: Storage,
		public loadingService: LoadingProvider,
		public alertCtrl: AlertController,
		public globalProvider: GlobalProvider,
		public menuCtrl: MenuController
	) {
		this.profiledetails = this.navParams.get("details");
		this.myDate = new Date(this.profiledetails.dob).toISOString();
		this.range = this.profiledetails.maxDistance;
		this.formData = this.formBuilder.group({
			firstname: ["", Validators.required],
			lastname: ["", Validators.required],
			desc: ["", Validators.required],
			range: ["", Validators.required],
			gender: [this.navParams.get("details").sex],
			myDate: ["", Validators.required],
			occupation: ["", Validators.required]
		});
	}

	ionViewWillEnter() {
		this.range = this.profiledetails.maxDistance;
	}
	showAlert() {
		let alert = this.alertCtrl.create({
			title: "Update failed!",
			subTitle: "Please try with correct details.",
			buttons: ["Try Again"]
		});
		alert.present();
	}

	dateformat(event: any) {
		console.log("=====date===", event);
	}

	async submitLoginForm() {
		var firstName = this.formData.value.firstname;
		var lastname = this.formData.value.lastname;
		var desc = this.formData.value.desc;
		console.clear();
		console.log(this.formData.value.desc);
		var range = this.formData.value.range;
		var myDate = new Date(this.formData.value.myDate);
		var myOccupation = this.formData.value.occupation;
		var gender = this.formData.value.gender;
		this.loadingService.showLoading("Editing");
		this.globalProvider
			.profileUpdate(
				gender,
				myOccupation,
				desc,
				firstName,
				lastname,
				myDate.getTime(),
				range
			)
			.subscribe(data => {
				this.loadingService.hideLoading();
				this.navCtrl.pop();
			});
	}
}
