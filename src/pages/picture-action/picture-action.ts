import { Component } from '@angular/core';
import {Events, IonicPage, NavController, NavParams, ViewController} from 'ionic-angular';

/**
 * Generated class for the PictureActionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-picture-action',
  templateUrl: 'picture-action.html',
})
export class PictureActionPage {

  constructor(public viewCtrl: ViewController,public events: Events,public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PictureActionPage');
  }
    makeProfile(){
        this.viewCtrl.dismiss();
        this.events.publish('action1', 'makeprofile');

    }
    deleteImage(){
        this.viewCtrl.dismiss();
        this.events.publish('action2', 'makeprofile');
    }

}
