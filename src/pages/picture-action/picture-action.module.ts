import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PictureActionPage } from './picture-action';

@NgModule({
  declarations: [
    PictureActionPage,
  ],
  imports: [
    IonicPageModule.forChild(PictureActionPage),
  ],
})
export class PictureActionPageModule {}
