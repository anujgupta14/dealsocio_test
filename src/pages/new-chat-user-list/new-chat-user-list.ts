import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Navbar } from 'ionic-angular';
import { GlobalProvider } from '../../providers/global/global';
import {ChatWindPage} from "../chat-window/chat-window";

/**
 * Generated class for the NewChatUserListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-new-chat-user-list',
  templateUrl: 'new-chat-user-list.html',
})
export class NewChatUserListPage {
	@ViewChild(Navbar) navBar: Navbar;
	public users = [];
	search_text: string = "";
	public baseURL = this.globalProvider.baseURL;

  constructor(public navCtrl: NavController, public navParams: NavParams, public globalProvider: GlobalProvider) {
  	this.getNewChatUserList();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NewChatUserListPage');
    this.navBar.backButtonClick = (e:UIEvent)=>{
    	this.navCtrl.pop();
	}
  }

  getNewChatUserList(){
    this.globalProvider.newChatUserList(this.globalProvider.userDetails.session.userId)
        .subscribe(data => {
        	console.log(data);
            this.users = data.userDtoList;
        });
    }

    goToChatWindow(userId){
		this.navCtrl.push('ChatWindPage', {
        	userId : userId
      	});
    }

}
