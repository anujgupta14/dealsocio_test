import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NewChatUserListPage } from './new-chat-user-list';
//import {FilterPipe} from "../../providers/global/filter";
import {PipesModule} from "../../pipes/pipes.module";

@NgModule({
  declarations: [
  ],
  imports: [
    IonicPageModule.forChild(NewChatUserListPage),
      PipesModule
  ],
})
export class NewChatUserListPageModule {}
