import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NewchatlistPage } from './newchatlist';
import {PipesModule} from "../../pipes/pipes.module";

@NgModule({
  declarations: [
    NewchatlistPage,
  ],
  imports: [
    IonicPageModule.forChild(NewchatlistPage),
      PipesModule
  ],
})
export class NewchatlistPageModule {}
