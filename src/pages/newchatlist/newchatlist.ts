import {Component, ViewChild} from '@angular/core';
import {IonicPage, Navbar, NavController, NavParams} from 'ionic-angular';
import {GlobalProvider} from "../../providers/global/global";
import {ChatWindPage} from "../chat-window/chat-window";

/**
 * Generated class for the NewchatlistPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-newchatlist',
  templateUrl: 'newchatlist.html',
})
export class NewchatlistPage {

    @ViewChild(Navbar) navBar: Navbar;
    public users = [];
    search_text: string = "";
    public baseURL = this.globalProvider.baseURL;

    constructor(public navCtrl: NavController, public navParams: NavParams, public globalProvider: GlobalProvider) {
        this.getNewChatUserList();
    }

    ionViewDidLoad() {
        this.navBar.backButtonClick = (e:UIEvent)=>{
            this.navCtrl.pop();
        }
    }

    getNewChatUserList(){
        this.globalProvider.newChatUserList(this.globalProvider.userDetails.session.userId)
            .subscribe(data => {
                this.users = data.userDtoList;
            });
    }

    goToChatWindow(userId){
        this.navCtrl.push('ChatWindPage', {
            userId : userId
        });
    }

}
