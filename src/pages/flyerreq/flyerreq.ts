import { Component } from '@angular/core';
import {IonicPage, NavController, NavParams, AlertController, MenuController, Events} from 'ionic-angular';
import { GlobalProvider } from '../../providers/global/global';
import { Storage } from '@ionic/storage';
/**
 * Generated class for the FlyerreqPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-flyerreq',
  templateUrl: 'flyerreq.html',
})
export class FlyerreqPage {
  public reqlist : any=[];
  public Fid = this.navParams.get("flyerId");
  public baseURL = this.globalProvider.baseURL;
 constructor(public navCtrl: NavController,public events: Events, public navParams: NavParams,public globalProvider: GlobalProvider, public storage: Storage, public alertCtrl: AlertController, public menuCtrl: MenuController) {
  }

  ionViewDidLoad() {
    	console.log('ionViewDidLoad ConreqPage');
		this.globalProvider.getlistrequestbyid(this.Fid)
        .subscribe(data => {
            this.reqlist = data.userDtoList;
            console.log(data);
            console.log("Connection Requests",this.reqlist)
        });
    }
    ionViewWillLeave(){
        console.log("inside will leave ");
        this.events.publish('user:loggedIn', "");
    }
    actionReq(id,status){
      this.globalProvider.acceptrejectflyer(this.Fid,id,status)
        .subscribe(data => {
            console.log(data);
            console.log("Responese",data);
            this.ionViewDidLoad();
        });
    }
    getinfo(id){
          this.navCtrl.push('UserDetailPage', {
          userid : id
      });
    }

}
