import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FlyerreqPage } from './flyerreq';

@NgModule({
  declarations: [
    FlyerreqPage,
  ],
  imports: [
    IonicPageModule.forChild(FlyerreqPage),
  ],
})
export class FlyerreqPageModule {}
