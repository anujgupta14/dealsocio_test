import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import $ from "jquery";
import {LoadingProvider} from "../../providers/loading/loading";
import {GlobalProvider} from "../../providers/global/global";
import {Storage} from "@ionic/storage";
import {TabsPage} from "../tabs/tabs";

/**
 * Generated class for the VerifyAccountPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-verify-account',
  templateUrl: 'verify-account.html',
})
export class VerifyAccountPage {
    fromPage:any=this.navParams.get('pageName');
    passcodeForm: FormGroup;
    verifyvalue:any="";
    phoneNo : any= this.navParams.get('phoneNo');
  constructor(public storage: Storage,public loadingService:LoadingProvider,public globalProvider: GlobalProvider,public formBuilder: FormBuilder,public navCtrl: NavController, public navParams: NavParams) {
      this.passcodeForm = formBuilder.group({
          'one': ['', Validators.required],
          'two': ['', Validators.required],
          'three': ['', Validators.required],
          'four': ['', Validators.required]
      });
  }
    called(){
        $(".inputs").keyup(function () {
            console.log("keyup");
            if (this.value.length == this.maxLength) {
                $(this).next('.inputs').focus();
            }
        });
    }
  ionViewDidLoad() {
    console.log('ionViewDidLoad VerifyAccountPage');
      if(this.fromPage){
          this.verifyvalue=this.globalProvider.manualLoginData();
          console.log(this.verifyvalue);
      }else {
          this.verifyvalue=this.globalProvider.signupData();
          console.log(this.verifyvalue);
      }
  }
    submitOtpForm(otpvalue) {

        let finalPasscode=otpvalue.one+""+otpvalue.two+otpvalue.three+otpvalue.four;
        this.loadingService.showLoading("Verifying");
        this.verifyvalue.otp =finalPasscode;
        //this.verifyvalue.find((item) => console.log(item));
        this.loadingService.hideLoading();
        if(this.fromPage){
            this.globalProvider.manualLogin(this.verifyvalue).subscribe(
                (data) => {
                    this.loadingService.hideLoading();
                    if (data.successful) {
                        localStorage.setItem('myuserToken', data.session.token);
                        localStorage.setItem('myuserUserId', data.session.userId);
                        this.storage.set('userDetails', data).then((response) => {
                            console.log(response);
                            this.storage.get('userDetails').then((data) => {
                                if (data) {
                                    this.globalProvider.userDetails = data;
                                    this.globalProvider.headers.delete("token");
                                    this.globalProvider.headers.delete("userid");
                                    this.globalProvider.headers.append("token", data.session.token);
                                    this.globalProvider.headers.append("userid", data.session.userId);
                                    this.navCtrl.setRoot(TabsPage);
                                }
                            })
                        })
                    } else {
                        this.loadingService.showNotification(data.message, "wrong", "");
                    }
                    console.log(data);
                });
        }
        else {
            this.globalProvider.signUp(this.verifyvalue).subscribe(
                (data) => {
                    this.loadingService.hideLoading();
                    if (data.successful) {
                        localStorage.setItem('myuserToken', data.session.token);
                        localStorage.setItem('myuserUserId', data.session.userId);
                        this.storage.set('userDetails', data).then((response) => {
                            console.log(response);
                            this.storage.get('userDetails').then((data) => {
                                if (data) {
                                    this.globalProvider.userDetails = data;
                                    this.globalProvider.headers.delete("token");
                                    this.globalProvider.headers.delete("userid");
                                    this.globalProvider.headers.append("token", data.session.token);
                                    this.globalProvider.headers.append("userid", data.session.userId);
                                    this.navCtrl.setRoot(TabsPage);
                                }
                            })
                        })
                    } else {
                        this.loadingService.showNotification(data.message, "wrong", "");
                    }
                    console.log(data);
                });
        }
        }
    resend(data) {
        let myformValue={
            "phoneNo" : this.phoneNo,                // mandatory
            "isdCode" : "91",                               // mandatory
            "otp" : ""                                    // not mandatory
        };
        this.loadingService.showLoading("Authenticating...");
        this.globalProvider.manualLogin(myformValue).subscribe(
            (response)=>{
                this.loadingService.hideLoading();
                if(response.successful){
                    this.loadingService.showNotification("please check message","right","");
                }else{
                    this.loadingService.showNotification(response.message,"wrong","");
                }
            });
    }
}
