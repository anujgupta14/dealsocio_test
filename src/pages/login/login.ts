import { Component} from '@angular/core';
import { IonicPage, NavController, NavParams,Platform} from 'ionic-angular';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook';
import { GlobalProvider } from '../../providers/global/global';
import { Storage } from '@ionic/storage';
import { TabsPage } from '../tabs/tabs';
import { Geolocation } from '@ionic-native/geolocation';
//import { GooglePlus } from '@ionic-native/google-plus';
//import {Firebase} from '@ionic-native/firebase';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {LoadingProvider} from "../../providers/loading/loading";
import {Diagnostic} from "@ionic-native/diagnostic";
import {AndroidPermissions} from "@ionic-native/android-permissions";
import {LocationAccuracy} from "@ionic-native/location-accuracy";
import {FCM} from "@ionic-native/fcm";

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})

export class LoginPage {
    public userInfo :any;
    loginForm: FormGroup;
    public locationdata : any;
    public dataList = [] ;
    firebaseToken: any;
  constructor(private fb: Facebook,private fcm: FCM,private diagnostic: Diagnostic,private locationAccuracy: LocationAccuracy,private androidPermissions: AndroidPermissions,public loadingService:LoadingProvider,public formBuilder: FormBuilder,public navCtrl: NavController, public navParams: NavParams,
    public globalProvider: GlobalProvider, public storage: Storage,private geolocation: Geolocation,
    public platform:Platform) {
      this.loginForm =  this.formBuilder.group({
          'phone': ['', Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(10)])],
      });
          this.storage.get('userDetails').then((data) => {
            if(data){
             console.log("inside constructor")
             console.log(data);
             this.globalProvider.getexper()
              .subscribe(data => {
                  this.dataList = data.elements;
                  console.log(this.dataList);
                  this.storage.set('exp',this.dataList[0])
              });

             this.nextpage();
             this.geolocation.getCurrentPosition().then((resp) => {
               this.locationdata = resp;
               this.globalProvider.location = resp;
                 localStorage.setItem('latitude',JSON.stringify(resp.coords.latitude));
                 localStorage.setItem('longitude',JSON.stringify(resp.coords.longitude));
              }).catch((error) => {
                console.log('Error getting location', error);
              })
            }
           });


        platform.ready().then(() => {
            this.fcm.getToken()
                .then(token => {
                        this.firebaseToken = token;
                    console.log('TOKEN on Launch ',this.firebaseToken);
                        this.globalProvider.fcmRegister(this.firebaseToken)
                            .subscribe(data => {
                                console.log('Token Register: ',data);
                            });
                    }
                )
                .catch(error => console.error('Error getting token', error));

            this.fcm.onTokenRefresh()
                .subscribe((token: string) => {
                        this.firebaseToken = token
                        console.log('TOKEN REFRESH______ ',this.firebaseToken);

                        this.globalProvider.fcmRegister(this.firebaseToken)
                            .subscribe(data => {
                                console.log('Token Register: ',data);
                            });
                    }
                );
        });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  facebooklogin()
  {
  	this.fb.login(['public_profile', 'email'])
 	 .then((res: FacebookLoginResponse) => this.apicall(res))
  		.catch(e => console.log('Error logging into Facebook', e));
  }

  // login(){
  // console.log("insidr login function");
  //    this.googlePlus.login({
  //        'webClientId': '82790890969-p2srp06ihks88ar0v6hc4q0mk3je66j3.apps.googleusercontent.com',
  //        'offline': true
  //    })
  //          .then(res => {
  //          alert("inside sucess");
  //       console.log(res);
  //     console.log('google + response', res);
  //     this.apicallGoogle(res);
  //     //this.navCtrl.setRoot(TabsPage);
  //   },err => alert(err))
  //   .catch(err => alert(err));
  // }

  apicall(res){
		this.globalProvider.userloginfacebook(res.authResponse.accessToken)
        .subscribe(data => {
            this.userInfo = data;
            this.storage.set('userDetails', data).then((response) => {
                console.log(response);
              this.storage.get('userDetails').then((data) => {
              if(data){
                  this.globalProvider.userDetails = data;
                  this.globalProvider.headers.delete("token");
                  this.globalProvider.headers.delete("userid");
                  this.globalProvider.headers.append("token",data.session.token);
                  this.globalProvider.headers.append("userid",data.session.userId);
                  // this.globalProvider.getexper().subscribe(exdata =>{
                  //   console.log("experiance list called");
                  //   this.storage.set('exp',exdata.elements[0])
                  //   console.log(exdata);
                  // });

                  // this.globalProvider.fcmRegister(this.firebaseToken)
		           //  .subscribe(fcmdata => {
		           //      console.log('Token Register: ',fcmdata);
		           //  });
              }
                this.nextpage();
              })
            })
       });
  }
  apicallGoogle(res){
      console.log(res);
		this.globalProvider.userloginGoogle(res.accessToken)
        .subscribe(data => {

            this.userInfo = data;
            this.storage.set('userDetails', data).then(() => {
              this.storage.get('userDetails').then((data) => {
              if(data){

                  this.globalProvider.userDetails = data;
                  this.globalProvider.headers.delete("token");
                  this.globalProvider.headers.delete("userid");
                  this.globalProvider.headers.append("token",data.session.token);
                  this.globalProvider.headers.append("userid",data.session.userId);
                  this.globalProvider.getexper().subscribe(exdata =>{
                    this.storage.set('exp',exdata.elements[0]);
                  });

                  this.globalProvider.fcmRegister(this.firebaseToken)
		            .subscribe(fcmdata => {
		                console.log('Token Register: ',fcmdata);
		            });
              }
                this.nextpage();
              })
            })
       });
  }

  nextpage()
  {
      this.navCtrl.push(TabsPage);
  }

  updateTerm(checkbox) {
    // console.log(checkbox.checked);
  }
    manualLogin(data) {
        let myformValue={
            "phoneNo" : data.value.phone,                // mandatory
            "isdCode" : "91",                               // mandatory
            "otp" : ""                                    // not mandatory
        };
        console.log(data.value);
        this.loadingService.showLoading("Authenticating...");
        this.globalProvider.manualLogin(myformValue).subscribe(
            (response)=>{
                this.loadingService.hideLoading();
                if(response.successful){
                    this.navCtrl.push("VerifyAccountPage",{'pageName':'login','phoneNo':data.value.phone});
                }else{
                    this.loadingService.showNotification(response.message,"wrong","");
                }
            });
    }
    onSignup(){
        this.navCtrl.push('SignupPage');
    }
}
