import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OtpverPage } from './otpver';

@NgModule({
  declarations: [
    OtpverPage,
  ],
  imports: [
    IonicPageModule.forChild(OtpverPage),
  ],
})
export class OtpverPageModule {}
