import { Component } from '@angular/core';
import {IonicPage, NavController,NavParams,AlertController,MenuController,ToastController} from 'ionic-angular';
import {Validators, FormBuilder, FormGroup} from '@angular/forms';
import { GlobalProvider } from '../../providers/global/global';
import { Storage } from '@ionic/storage';
import $ from 'jquery';
import {LoadingProvider} from "../../providers/loading/loading";

@IonicPage()
@Component({
  selector: 'page-otpver',
  templateUrl: 'otpver.html',
})
export class OtpverPage {
  public formData : any;
  public userinfo : any;
    passcodeForm: FormGroup;
  constructor(public navCtrl: NavController,public loadingService:LoadingProvider,private toastCtrl: ToastController, public navParams: NavParams, public formBuilder: FormBuilder,public globalProvider: GlobalProvider, public storage: Storage, public alertCtrl: AlertController, public menuCtrl: MenuController) {
  	  this.formData = this.formBuilder.group({
        phone: ['', Validators.required]
      });
      this.passcodeForm = formBuilder.group({
          'one': ['', Validators.required],
          'two': ['', Validators.required],
          'three': ['', Validators.required],
          'four': ['', Validators.required]
      });
      this.storage.get('userDetails').then((data) => {
        //console.log(data);
          this.userinfo = data;
      });
  }

    called(){
        $(".inputs").keyup(function () {
            console.log("keyup");
            if (this.value.length == this.maxLength) {
                $(this).next('.inputs').focus();
            }
        });
    }

  showAlert() {
      let alert = this.alertCtrl.create({
      title: 'Verification failed!',
      subTitle: 'Please try with correct details.',
      buttons: ['Try Again']
    });
      alert.present();
  }


  submitOtpForm(otpvalue) {
      let finalPasscode=otpvalue.one+""+otpvalue.two+otpvalue.three+otpvalue.four;
      this.loadingService.showLoading("Verifying");
    this.globalProvider.verifyotp(finalPasscode).subscribe(data => {
        if(data.successful){
            this.loadingService.hideLoading();
            let toast = this.toastCtrl.create({
                message: "Verified",
                duration: 2000,
                position: 'middle'
            });
            toast.onDidDismiss(() => {
                this.navCtrl.popTo(this.navCtrl.getByIndex(this.navCtrl.length() - 3));
            });
            toast.present();
        }
        else{
            this.loadingService.hideLoading();
          this.showAlert();
         }
    	});
	}
}
