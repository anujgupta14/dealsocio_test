import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,AlertController,MenuController } from 'ionic-angular';
import { GlobalProvider } from '../../providers/global/global';
import { Storage } from '@ionic/storage';
/**
 * Generated class for the FriendlistPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-friendlist',
  templateUrl: 'friendlist.html',
})
export class FriendlistPage {

  public connections : any=[];
  public userid : any;
  public baseURL = this.globalProvider.baseURL;
  constructor(public navCtrl: NavController, public navParams: NavParams,public globalProvider: GlobalProvider, public storage: Storage, public alertCtrl: AlertController, public menuCtrl: MenuController) {
    this.getlist();
  }
  getlist(){
    this.globalProvider.contactListById(this.globalProvider.userDetails.session.userId)
        .subscribe(data => {
            this.connections = data.contacts;
        });
    }

  userDetailPage(id){
          this.navCtrl.push('UserDetailPage', {
          userid : id
      });
  }

  removecontact(id){
    this.globalProvider.removeContactById(id)
        .subscribe(data => {
          console.log(data);
            this.getlist();
        });
  }

  goToChatWindow(chat){
    this.navCtrl.push('ChatWindPage', {
      userId : chat.id,
      userName : chat.name,
      userProfilePic : chat.image
    });
  }

}
