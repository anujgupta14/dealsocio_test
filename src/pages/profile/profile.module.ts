import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProfilePage } from './profile';
import { Camera } from '@ionic-native/camera';
import {SocialSharing} from "@ionic-native/social-sharing";
//import {Facebook} from "@ionic-native/facebook";
import { AppRate } from '@ionic-native/app-rate';

@NgModule({
  declarations: [
    ProfilePage,
  ],
  imports: [
    IonicPageModule.forChild(ProfilePage),
  ],
    providers: [
         Camera,
        SocialSharing,
        AppRate,
   //     Facebook
    ]
})
export class ProfilePageModule {}
