import { Component } from '@angular/core';
import { IonicPage,App,NavController} from 'ionic-angular';
import { LoginPage } from '../login/login';
import { GlobalProvider } from '../../providers/global/global';
import { Storage } from '@ionic/storage';
import { SocialSharing } from '@ionic-native/social-sharing';
import {LoadingProvider} from "../../providers/loading/loading";
import { AppRate } from '@ionic-native/app-rate';
import {Facebook} from "@ionic-native/facebook";

/**
 * Generated class for the ProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {
  public userinfo = this.globalProvider.userDetails;
  public baseURL = this.globalProvider.baseURL;
  public profiledetails : any;
  public togglevalue : any;
  //public imageChange:any;
    public myprofilepic :any="";
    public i =0;
    public mytime:any=new Date().getTime() +(++this.i);
  //public finalData:any;
  public unreadCount: number = 0;
    public unreadNotify: number = 0;
  public base64Image:any;
  constructor(private fb: Facebook,private appRate: AppRate,public app: App,public loadingService:LoadingProvider,public navCtrl: NavController, public globalProvider: GlobalProvider, public storage: Storage, private socialSharing: SocialSharing,
    )
  {
     console.log(this.userinfo);

  }

  ionViewWillEnter() {
      this.loadingService.showLoading("Profile Detail");
        this.globalProvider.getmyprof()
        .subscribe(data => {
            this.profiledetails = data.user;
            this.myprofilepic=this.baseURL+this.profiledetails.profilePic+'?timestamp='+this.mytime+(++this.i);
            this.togglevalue=this.profiledetails.notificationFlag;
            this.loadingService.hideLoading();
        });
    console.log('ionViewDidLoad ProfilePage');
  }
    ionViewDidEnter(){
        this.globalProvider.getUnreadConversations()
            .subscribe(data => {
                this.unreadCount = data.count;
            });
        this.globalProvider.getUnreadNotification()
            .subscribe(data => {
                if(data.successful){
                    this.unreadNotify=data.count;
                }
            });
    }
    goToNotifications() {
        this.navCtrl.push('NotificationsPage');
    }
    goToChat(){
        this.navCtrl.push('NewchatPage');
    }
  navigateToProfileInfo() {
    this.navCtrl.push('ProfileInfoPage', {
      details : this.profiledetails
    });
  }
  navigateToGallery() {
    this.navCtrl.push('GalleryPage', {
      userId : this.userinfo.user.id, userImages: this.profiledetails.images
    });
  }

  navigateToUserList() {
    this.navCtrl.push('FriendlistPage');
  }
    navigateToFaq() {
        this.navCtrl.push('FaqPage');
    }
  navigateToblockList() {
    this.navCtrl.push('BlkdusrsPage');
  }

  navigateToReqList() {
    this.navCtrl.push('ConreqPage');
  }

    AboutPage(){
        this.navCtrl.push('AboutPage');
    }
  logout(){
    this.storage.clear();
    this.fb.logout();
    //this.googlePlus.logout();
    var nav = this.app.getRootNav();
    nav.setRoot(LoginPage);
  }
  deactivateAcc(){
      this.loadingService.showLoading("Deactivating");
    this.globalProvider.deactivateAcc().subscribe(data => {
        this.loadingService.hideLoading();
      this.logout();
    })
  }
  shareSheetShare() {
    this.socialSharing.share("Share App Everywhere", "Dealsocio", "URL to file or image", "https://play.google.com/store/apps/details?id=com.oiamigotech.dealsocio").then(() => {
      console.log("shareSheetShare: Success");
    }).catch(() => {
      console.error("shareSheetShare: failed");
    });
  }
   /* notification block code */
    update(info){
        console.log(info.checked);
        this.loadingService.showLoading("Updating");
        this.globalProvider.updateNotification(info.checked)
            .subscribe(data => {
                console.log(data);
                this.loadingService.hideLoading();
                if(data.successful){
                    this.loadingService.showNotification(data.message,"right","");
                }
            });
    }

    rateApp(){
        //this.appRate.navigateToAppStore();
        // or, override the whole preferences object
        // this.appRate.preferences = {
        //     usesUntilPrompt: 3,
        //     storeAppURL: {
        //         ios: '1216856883',
        //         android: 'market://details?id=com.moglix.runner',
        //         windows: 'ms-windows-store://review/?ProductId=<store_id>'
        //     }
        // };
        //
        // this.appRate.promptForRating(false);
        this.appRate.preferences = {
           displayAppName: 'Dealsocio',
            simpleMode: false,
            usesUntilPrompt: 5,
            promptAgainForEachNewVersion: false,
            inAppReview: true,
            storeAppURL: {
                ios: '1216856883',
                android: 'market://details?id=com.oiamigotech.dealsocio'
            },
            customLocale: {
                title: 'Do you enjoy %@?',
                message: 'If you enjoy using %@, would you mind taking a moment to rate it? Thanks so much!',
                cancelButtonLabel: 'No, Thanks',
                laterButtonLabel: 'Later',
                yesButtonLabel: 'Rate DealSocio',
                noButtonLabel: 'Cancel',
                rateButtonLabel: 'Rate Now',
                appRatePromptTitle: 'Please Rate Dealsocio',
            },
            callbacks: {
                onRateDialogShow: function(callback){
                    console.log('rate dialog shown!');
                },
                onButtonClicked: function(buttonIndex){
                    console.log('Selected index: -> ' + buttonIndex);
                },
                handleNegativeFeedback: function(){
                    window.open('mailto:anuj.mpec@gmail.com','_system');
                },
            }
        };

        // Opens the rating immediately no matter what preferences you set
        this.appRate.promptForRating(true);
    }
}
