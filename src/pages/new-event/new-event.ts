import { Component } from '@angular/core';
import {Events, IonicPage, NavController, NavParams} from 'ionic-angular';
import {GlobalProvider} from "../../providers/global/global";
/**
 * Generated class for the NewEventPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-new-event',
  templateUrl: 'new-event.html',
})
export class NewEventPage {
    page1: any = 'MyeventsPage';
    page2:any ='AcceptedeventsPage';
    public unreadCount: number = 0;
    public unreadNotify: number = 0;
  constructor(public events: Events,public globalProvider: GlobalProvider,public navCtrl: NavController, public navParams: NavParams) {
  }

    ionViewWillEnter() {
        this.globalProvider.makebackActive=false;
    console.log('ionViewDidLoad NewEventPage');
        this.events.publish('user:event', "");
      this.globalProvider.getUnreadConversations()
          .subscribe(data => {
              this.unreadCount = data.count;
          });
      this.globalProvider.getUnreadNotification()
          .subscribe(data => {
              if(data.successful){
                  this.unreadNotify=data.count;
              }
          });
  }
    goToChat(){
        this.globalProvider.makebackActive=true;
        this.navCtrl.push('NewchatPage');
    }
    goToNotifications() {
        this.globalProvider.makebackActive=true;
        this.navCtrl.push('NotificationsPage');
    }

}
