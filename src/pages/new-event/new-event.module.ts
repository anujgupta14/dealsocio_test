import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NewEventPage } from './new-event';
import {SuperTabsModule} from "ionic2-super-tabs";

@NgModule({
  declarations: [
    NewEventPage,
  ],
  imports: [
    IonicPageModule.forChild(NewEventPage),
      SuperTabsModule.forRoot(),
  ],
})
export class NewEventPageModule {}
