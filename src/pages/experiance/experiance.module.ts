import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ExperiancePage } from './experiance';

@NgModule({
  declarations: [
    ExperiancePage,
  ],
  imports: [
    IonicPageModule.forChild(ExperiancePage),
  ],
})
export class ExperiancePageModule {}
