import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GlobalProvider } from '../../providers/global/global';
import { Storage } from '@ionic/storage';
import {LoadingProvider} from "../../providers/loading/loading";
/**
 * Generated class for the ExperiancePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-experiance',
  templateUrl: 'experiance.html',
})
export class ExperiancePage {
	public userinfo = this.globalProvider.userDetails;
	public exLength = 0;
  public baseURL : any;
  public dataList = [] ;
  public localStorage = false;
    public unreadCount: number = 0;
    public unreadNotify: number = 0;
  constructor(public navCtrl: NavController,public loadingService:LoadingProvider, public navParams: NavParams,public globalProvider: GlobalProvider, public storage: Storage) {
     this.baseURL = this.globalProvider.baseURL;
     console.log(this.navParams.get('experianceName'));
     if(this.navParams.get('experianceName')!=undefined){
         this.exLength=1;
     }else{
         this.exLength=0;
     }
  }
    ionViewDidEnter(){
        this.loadingService.showLoading("Getting Experiance");
        this.globalProvider.getUnreadConversations()
            .subscribe(data => {
                this.unreadCount = data.count;
            });
        this.globalProvider.getUnreadNotification()
            .subscribe(data => {
                if(data.successful){
                    this.unreadNotify=data.count;
                }
            });
        if(this.navParams.get('experianceName')){
            this.newexper();
        }else{
            this.getexper();
        }

    }
    goToChat() {
        this.navCtrl.push('NewchatPage');
    }
    goToNotifications() {
        this.navCtrl.push('NotificationsPage');
    }
   getexper(){
		this.globalProvider.getexper()
        .subscribe(data => {
            this.loadingService.hideLoading();
            this.userinfo = data;
            this.dataList = data.elements;
        });
    }
    newexper(){
        this.globalProvider.newexper(this.navParams.get('storeId'))
            .subscribe(data => {
                this.loadingService.hideLoading();
                this.userinfo = data;
                this.dataList = data.elements;
            });
    }
    expdetail(exp){
        console.log(this.navParams.get("store"));
        console.log(exp);
        //alert(this.navParams.get("storeId"));
      if(this.navParams.get('experianceName')){
          console.log("in true")
          this.navCtrl.push("CreatePlanPage", {
              store: this.navParams.get('store'),
              experianceName:exp.displayName,
              experiance : exp
          });
      }else{
          console.log("in else")
          this.globalProvider.experiance = exp;
          this.storage.set('exp',exp);
          this.navCtrl.push('PlacefeedPage', {
              experiance : exp
          });
      }

    }
}
