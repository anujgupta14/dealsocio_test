import {animate, Component, state, style, transition, trigger} from '@angular/core';
import {IonicPage, NavController, NavParams, ToastController} from 'ionic-angular';
import {GlobalProvider} from "../../providers/global/global";

/**
 * Generated class for the UserEventPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-user-event',
  templateUrl: 'user-event.html',
    animations: [
        trigger('listItemState', [
            state('in',
                style({
                    opacity: 1,
                    height:'*',
                    minHeight: '*'
                })
            ),
            transition('* => void', [
                animate('0.25s', style({
                    opacity: 0,
                    height: '1px',
                    minHeight: '1px',
                    transform: 'perspective(500px) translateZ(-400px) scale(1.2)',
                }))
            ])
        ])
    ]
})
export class UserEventPage {
    public userinfo : any={};
    showAccord: Array<boolean> = [];
    public baseURL = this.globalProvider.baseURL;

  constructor(private toastCtrl: ToastController,public globalProvider: GlobalProvider,public navCtrl: NavController, public navParams: NavParams) {
      this.userinfo=this.navParams.get('uuid');
      this.showAccord = Array(this.userinfo.flyers.length).fill(false);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UserEventPage');
  }
    public uninterest(id,myindex) {
        this.userinfo.flyers.splice( myindex, 1 );
        this.globalProvider.avoidbyid(id)
            .subscribe(data => {
                if(data.successful){
                    console.log("successful done");
                    this._successCallback(data.message);
                }else{
                    this._errorCallback(data.errors[0].description);
                }
            });
    }
    public  interest(id,myindex) {
        this.userinfo.flyers.splice( myindex, 1 );
        this.globalProvider.getinterstbyid(id)
            .subscribe(data => {
                if(data.successful){
                    this._successCallback(data.message);
                }else{
                    this._errorCallback(data.errors[0].description);
                }
            });
    }
    public _successCallback(myMessage){
        let toast = this.toastCtrl.create({
            message: myMessage,
            duration: 1000,
            position: 'top'
        });
        toast.onDidDismiss(() => {
            console.log('Dismissed toast');
        });
        toast.present();
    }
    public _errorCallback(myError){
        let toast = this.toastCtrl.create({
            message: myError,
            duration: 2000,
            position: 'top'
        });
        toast.onDidDismiss(() => {
            console.log('Dismissed toast');
        });
        toast.present();
    }
    goToGoogleMap(mapData){
        window.open('geo:0,0?q='+mapData.coordinates[1]+','+mapData.coordinates[0], '_system');
    }
}
