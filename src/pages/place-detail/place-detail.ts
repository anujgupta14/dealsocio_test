import { Component } from "@angular/core";
import {
	IonicPage,
	NavController,
	NavParams,
	AlertController,
	MenuController
} from "ionic-angular";
import { GlobalProvider } from "../../providers/global/global";
import { Storage } from "@ionic/storage";
import {ExperiancePage} from "../experiance/experiance";

/**
 * Generated class for the PlaceDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
	selector: "page-place-detail",
	templateUrl: "place-detail.html"
})
export class PlaceDetailPage {
	public store_id = this.navParams.get("id");
	public baseURL = this.globalProvider.baseURL;
	public storedata: any = {};
	public Flyerslength: any;
	public tagIconUrls = [];
	constructor(
		public navCtrl: NavController,
		public navParams: NavParams,
		public globalProvider: GlobalProvider,
		public storage: Storage,
		public alertCtrl: AlertController,
		public menuCtrl: MenuController
	) {
		// this.storedata = {};
	}

	ionViewDidLoad() {
		console.log("ionViewDidLoad PlaceDetailPage");
		this.getstoredetail(this.store_id);
	}

	createPlan(store = " ") {
		console.log(this.storedata);
		//CreatePlanPage
		this.navCtrl.push("ExperiancePage", {
			store: this.storedata,
			experianceName: this.storedata.storeName,
			storeId:this.store_id
		});
	}

	getstoredetail(sid) {
		this.globalProvider.getstorebyid(sid).subscribe(data => {
			this.storedata = data.storeDto;
			if (this.storedata) {
				this.Flyerslength = this.storedata.activeFlyers.length;
				for (let tag of this.storedata.tags) {
					let url = "assets/icons/" + tag + ".png";
					this.tagIconUrls.push(url);
				}
			}
		});
	}

	getsana(url) {
		console.clear();
		console.log("======================================");
		console.log(url);
		console.log("======================================");
		return this.globalProvider.sanatizeurl(url);
	}

	upldateLoc() {
		this.navCtrl.push("SellocationPage");
	}
}
