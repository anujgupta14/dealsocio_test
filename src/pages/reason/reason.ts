import { Component } from '@angular/core';
import {IonicPage, NavController, NavParams, ToastController} from 'ionic-angular';
import {GlobalProvider} from "../../providers/global/global";
import {LoadingProvider} from "../../providers/loading/loading";

/**
 * Generated class for the ReasonPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-reason',
  templateUrl: 'reason.html',
})
export class ReasonPage {
    reasonList:any=[];
    myReason:any="";
    optionalReason:any="";
    showOption:boolean=false;
  constructor(public navCtrl: NavController,private toastCtrl: ToastController,public loadingService:LoadingProvider, public navParams: NavParams,public globalProvider: GlobalProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ReasonPage');
  }
  ionViewWillEnter(){
      this.reasonList=[{
        'id':'Spam',
          'value':'Spam'
      },{
          'id':'Rumors and Fraud',
          'value':'Rumors and Fraud'
      },{
          'id':'Inappropriate Behaviour',
          'value':'Inappropriate Behaviour'
      },{
          'id':'Nudity and Pornography',
          'value':'Nudity and Pornography'
      },{
          'id':'Violence or Self Injury',
          'value':'Violence or Self Injury'
      },{
          'id':'Hate Symbols',
          'value':'Hate Symbols'
      },{
          'id':'others',
          'value':'others'
      }];
  }
    selected(indexNo){
      console.log(this.myReason);
      if(this.myReason=='others'){
          this.showOption=true;
      }else {
          this.showOption=false;
      }
    }
    provideReason(){
        console.log(this.myReason);
        if(!this.myReason){
            alert("atleast some reason required");
        }else{
            console.log(this.optionalReason);
            console.log(this.navParams.get('id'));
            console.log(this.navParams.get('flyerid'));
            if(this.navParams.get('flyerid')==undefined){
              //  this.navParams.get('flyerid')=null;
            }
            if(this.myReason=='others'){
                this.myReason=this.optionalReason;
            }
            this.globalProvider.reportUserById(this.navParams.get('id'),this.myReason,this.navParams.get('flyerid'))
                .subscribe(data => {
                    console.log(data);
                    if(data.successful){
                        let toast = this.toastCtrl.create({
                            message: data.message,
                            duration: 2000,
                            cssClass: "right",
                            position: 'bottom'
                        });
                        toast.onDidDismiss(() => {
                            console.log('Dismissed toast');
                            this.navCtrl.pop();
                        });
                        toast.present();
                    }else{
                        this.loadingService.showNotification(data.message,"right","");
                    }
                    // if(data.status==500){
                    //     var a=JSON.parse(data._body);
                    //     this.loadingService.showNotification(a.message,"wrong","")
                    // }
                });
        }

    }
}
