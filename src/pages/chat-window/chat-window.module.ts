import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import {Camera} from "@ionic-native/camera";
import { ChatWindPage} from "./chat-window";
//import { AgmCoreModule } from '@agm/core';

@NgModule({
  declarations: [
      ChatWindPage,
  ],
  imports: [
    IonicPageModule.forChild(ChatWindPage),
  ],
  schemas:  [ CUSTOM_ELEMENTS_SCHEMA ],
    providers: [
        Camera
    ]
})
export class ChatWindowPageModule {}
