import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, ActionSheetController, ToastController, Content, LoadingController,ModalController,AlertController,FabContainer } from 'ionic-angular';
import { GlobalProvider } from '../../providers/global/global';
import { Camera, CameraOptions } from '@ionic-native/camera';
import {FileTransfer, FileTransferObject, FileUploadOptions} from '@ionic-native/file-transfer';
import { Observable } from 'rxjs/Observable';
import {Message} from '@stomp/stompjs';
import { Subscription } from 'rxjs/Subscription';
import { DatePipe } from '@angular/common';
import * as SockJS from 'sockjs-client';
import {StompRService, StompConfig} from '@stomp/ng2-stompjs';
import {LoadingProvider} from "../../providers/loading/loading";

/**
 * Generated class for the ChatWindowPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-chat-window',
  templateUrl: 'chat-window.html',
})
export class ChatWindPage {
  @ViewChild('content') content: Content;
  @ViewChild('fab') fab: FabContainer;
  public myInfo = this.globalProvider.userDetails;
  public myId : any;
  public userinfo : any;
  public userId = this.navParams.get('userId');
  public userName = this.navParams.get('userName') ? this.navParams.get('userName') : '';
  public profilePic = this.navParams.get('userProfilePic') ? this.navParams.get('userProfilePic') : '';
  public baseURL = this.globalProvider.baseURL;
  public msgText = "";
  chatMessages = [];
  emojiToggled: boolean = false;
  emojitext: string;

  imageURI:any;
  valid:boolean = true;

  private subscription: Subscription;
  public messages: Observable<Message>;
  public subscribed: boolean;
  pageNumber:number = 1;
  infiniteEnabled: boolean = true;
  isChatListVisible: boolean = false;
  public blockedlist : any;public config: StompConfig;

  profileView:boolean = true;

  constructor(private transfer: FileTransfer,public loadingService:LoadingProvider,public navCtrl: NavController, public navParams: NavParams,public globalProvider: GlobalProvider, public actionSheetCtrl: ActionSheetController, private camera: Camera, public loading: LoadingController, public modalCtrl: ModalController, private _stompService: StompRService, private alertCtrl: AlertController, private toastCtrl: ToastController) {
    this.getMessages({userId: this.userId, pagination: {pageNumber: this.pageNumber, pageSize: 20} });
    //this.scrollto();
    this.globalProvider.markRead(this.userId).subscribe(data => {});
  }

  ionViewDidLoad() {
    if((this.userName != '') && (this.profilePic != '')){
      this.profilePic = this.profilePic;
    } else {
      this.getuserbyid(this.userId);
    }

    this.myId = this.myInfo.session.userId;
    this.getuserbyid(this.myId, true);
    this.socketConnect();
    this.getBlockList();

    this.subscribed = false;
    this.subscribe();
  }

  ionViewDidLeave(){
    this.subscribed = false;
    this.subscription.unsubscribe();
  }
 public socketConnect(){
      this.config = {
  		    url: new SockJS('https://api.dealsocio.com/chat-socket?userid=' + this.myInfo.session.userId + "&token=" +this.myInfo.session.token),
  		    headers: {
  		      token: this.myInfo.session.token,
  		      userid: this.myInfo.session.userId
  		    },
  		    heartbeat_in: 0, // Typical value 0 - disabled
  		    heartbeat_out: 10000, // every 20 seconds
  		    // Wait in milliseconds before attempting auto reconnect
  		    // Set to 0 to disable
  		    // Typical value 5000 (5 seconds)
  		    reconnect_delay: 5000,
  		    debug: true
  		};

  		  this._stompService.config = this.config;
  		  this._stompService.initAndConnect();
  }
  public subscribe() {
    if (this.subscribed) {
      return;
    }

    // Stream of messages
    this.messages = this._stompService.subscribe('/user/dealsocio/getChat');

    console.log('in subscribe');

    // Subscribe a function to be run on_next message
    this.subscription = this.messages.subscribe(this.on_next, (error)=>{
      console.log('error in subscribing');
    });

    this.subscribed = true;
  }

  /** Consume a message from the _stompService */
  public on_next = (message: Message) => {

    console.log('in on_next------------------');

    let msgBody = JSON.parse(message.body);

    if(msgBody.senderId == this.userId){
      let dataType = null;
      let data = null;
      if(msgBody.hasOwnProperty('dataType')){
        dataType = msgBody.dataType;
        data = msgBody.data;
      }

      this.chatMessages.push( {
        message: msgBody.message,
        data: data,
        dataType: dataType,
        senderId: this.userId,
        createdOn: msgBody.createdOn,
        readOn: null,
        profilePic: this.profilePic
      } );
      this.scrollto();
      //this.content.scrollToBottom();

      this.globalProvider.markRead(this.userId)
      .subscribe(data => {
        // dont handle anything here
      });
    }
  }

  	getBlockList(){
		this.globalProvider.blockList()
        .subscribe(data => {
        	for (var _i = 0; _i < data.contacts.length; _i++) {
		        if(data.contacts[_i].id == this.userId){
		        	this.profileView = false;
		        	break;
		        }
		    }
        });
    }

  getuserbyid(id, me=false){
    this.globalProvider.getuserbyid(id)
    .subscribe(data => {
      console.log(data);
      if(me){
        this.myInfo.profilePic = data.user.profilePic;
      } else {
        if(data.user){
          this.userinfo = data.user;
          this.userName = data.user.firstName + " " + data.user.lastName;
          this.profilePic = data.user.profilePic;
        } else {
          this.userinfo = null;
          this.userName = "USER";
          this.profilePic = null;
          this.valid = false;
          this.profileView = false;
        }
      }
    });
  }

  getMessages(params){
    this.globalProvider.getMessages(params)
    .subscribe(data => {
      this.valid = data.valid;
      if (typeof data.chatMessages !== 'undefined' && data.chatMessages.length > 0) {
        this.chatMessages = data.chatMessages.reverse();
        this.infiniteEnabled = true;
      } else {
        this.infiniteEnabled = false;
      }
      this.scrollto();
      /*setTimeout(() => {
          this.content.scrollToBottom();
       }, 1000);*/

      setTimeout(() => {
        console.log('visiblity change');
        this.isChatListVisible = true;
      }, 500);
    });
  }

  goToProfile(userId){
    if (typeof this.fab != "undefined") {
	    this.hideFab();
	}

    if(this.profileView){
      this.navCtrl.push('UserDetailPage', {
          userid : userId
      });
    }
  }

    report(id,message){
      this.showAlert(id,message);
    }

  blockUserById(id){
    this.globalProvider.blockUserById(id)
    .subscribe(data => {
      this.valid = false;
      this.profileView = false;
    });
  }

  sendMessage(dataUp=null,dataType=null){
    if (typeof this.fab != "undefined") {
      this.hideFab();
    }

    /*let loader = this.loading.create({
      content: "Sending..."
    });
    loader.present();*/

    let msgBody = {
      userId    : this.userId,
      message   : this.msgText,
      data      : dataUp,
      dataType  : dataType
    };
    this.globalProvider.sendMessages(msgBody)
    .subscribe(data => {
      console.log('Send Message result: ', dataUp);

      this.chatMessages.push( {
        message: this.msgText,
        data: dataUp,
        dataType: dataType,
        senderId: this.myId,
        createdOn: Date.now(),
        readOn: null,
        profilePic: this.myInfo.profilePic
      } );

      this.scrollto();
      //this.content.scrollToBottom();
      this.msgText = '';

      if(dataType == 'image'){
          this.navCtrl.push('ShowPicturePage', {
                pictureSrc: this.baseURL + dataUp
              });
        // const modal = this.modalCtrl.create('ShowPicturePage', {
        //   pictureSrc: this.baseURL + dataUp
        // });
        //
        // modal.present();
      }

      /*if(dataType == 'image' || dataType == 'location'){
        this.pageNumber = 1;
        this.getMessages({userId: this.userId, pagination: {pageNumber: this.pageNumber, pageSize: 20} });
        console.log('IN CONTENT RESIZE BECOZ IMAGE UPLOAD');
        this.content.resize();
        this.scrollto();
      }*/

      //loader.dismiss();

    });
  }

  scrollto() {
      setTimeout(() => {
        console.log('scrolling...');
        this.content.scrollToBottom();
      }, 500);
  }

  emojiHandleSelection(event) {
    this.msgText = this.msgText + event.char;
  }


    public getImage() {
      if (typeof this.fab != "undefined") {
        this.hideFab();
      }

      const options: CameraOptions = {
        quality: 80,
        destinationType: this.camera.DestinationType.FILE_URI,
        sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
        targetWidth: 640,
        targetHeight: 720
      }

      this.camera.getPicture(options).then((imageData) => {
        this.imageURI = imageData;
        //this.uploadFile();
          this.uploadImage(this.imageURI);
      }, (err) => {
        console.log(err);
      });
    }

public uploadImage (uriImage){
    const fileTransfer: FileTransferObject = this.transfer.create();
    let filename = uriImage.split('/').pop();
    let options1: FileUploadOptions = {
        fileKey: "file",
        fileName: filename,
        chunkedMode: false,
        mimeType: "multipart/form-data",
        params : {'counterUserId':this.userId},
        headers: {
            "userid": this.myInfo.session.userId,
            "token": this.myInfo.session.token,
        },
    };
    fileTransfer.upload(this.imageURI, this.globalProvider.serverURL + "/chat/addFile", options1)
        .then((data) => {
            console.log(data);
            this.loadingService.hideLoading();
            var test= JSON.parse(data.response);
            if(test.successful){
                this.loadingService.showNotification(test.message,'success','');
                this.sendMessage(test.filePath, "image");
            }else{
                this.loadingService.popNotification("Something went wrong in image upload",'success');
            }
        }, (err) => {
            this.loadingService.hideLoading();
            console.log(err);
            console.error(err);
        });
    // this.globalProvider.uploadchatImage(uriImage, options1).then(data => {
    //     var test= JSON.parse(data.response);
    //     console.log(test)
    //     if(test.successful){
    //         let toast = this.toastCtrl.create({
    //             message: test.message,
    //             duration: 2000,
    //             position: 'middle'
    //         });
    //         toast.onDidDismiss(() => {
    //             console.log('Dismissed toast');
    //              this.sendMessage(test.filePath, "image");
    //             //this.navCtrl.pop();
    //         });
    //         toast.present();
    //     }else{
    //         let toast = this.toastCtrl.create({
    //             message: "Something went wrong in image upload",
    //             duration: 2000,
    //             position: 'middle'
    //         });
    //         toast.onDidDismiss(() => {
    //             console.log('Dismissed toast');
    //             this.navCtrl.pop();
    //         });
    //         toast.present();
    //     }
    // });
}

    public takePicture() {
      /* if (typeof this.fab != "undefined") {
        this.hideFab();
      }  */

      const options: CameraOptions = {
        quality: 60,
        destinationType: this.camera.DestinationType.FILE_URI,
        encodingType: this.camera.EncodingType.JPEG,
        mediaType: this.camera.MediaType.PICTURE,
        targetWidth: 640,
        targetHeight: 720
      }

      this.camera.getPicture(options).then((imageData) => {
        this.imageURI = imageData;
        //this.uploadFile();
        this.uploadImage(this.imageURI);
      }, (err) => {
        // Handle error
      });
    }

    openLocationShare() {
      console.log("open share ")
     /* if (typeof this.fab != "undefined") {
        this.hideFab();
      } */
       this.navCtrl.push('LocationsharePage', { userId: this.userId });
      //  let locationShareModal = this.modalCtrl.create('LocationsharePage', { userId: this.userId });
      //  locationShareModal.onDidDismiss((location) => {
      //    if (!location) {
      //      return;
      //    }
      //
      //    console.log("location: ", location);
      //    this.sendMessage(location.lng+","+location.lat, "location");
      //  });
      // locationShareModal.present();
    }

    getLocation(locationString: string): {} {
      const splitted = locationString.split(',').map(Number);
      return {
        lng: splitted[0],
        lat: splitted[1],
        zoom: 22
      };
    }

    getStaticMap(locationString: string) {
    	const splitted = locationString.split(',').map(Number);
    	return "https://maps.googleapis.com/maps/api/staticmap?size=200x150&markers="+splitted[1]+","+splitted[0]+"&key=AIzaSyBu9aTfbPH5zurE0tR9cLUTdLB_j5wVX9M";
    }

    showPicture({ target }: Event) {
      if (typeof this.fab != "undefined") {
        this.hideFab();
      }

        this.navCtrl.push('ShowPicturePage', {
            pictureSrc: (<HTMLImageElement>target).src.slice(0, -4)
        });
     /*  const modal = this.modalCtrl.create('ShowPicturePage', {
         pictureSrc: (<HTMLImageElement>target).src.slice(0, -4)
       });

      modal.present();  */
    }

    showMessageMeta(item){
      if (typeof this.fab != "undefined") {
        this.hideFab();
      }

      let msg = '';

      const datePipe = new DatePipe('en-US');

      if(item.createdOn){
        msg += 'Sent: '+ datePipe.transform(item.createdOn, 'd MMM y, h:mm:ss a');
      }
      if(item.readOn){
        msg += '<br>Read: '+ datePipe.transform(item.readOn, 'd MMM y, h:mm:ss a');
      }
      let alert = this.alertCtrl.create({
        title: 'Message Info',
        subTitle: msg,
        buttons: ['Dismiss']
      });
      alert.present();
    }

    hideFab(onlyKb=false){
      if(onlyKb){
        (<HTMLElement>document.querySelector("ion-footer")).setAttribute('style', 'bottom: 0');
      } else {
        (<HTMLElement>document.querySelector("ion-footer")).setAttribute('style', 'bottom: 0');
        if (typeof this.fab != "undefined") {
          this.fab.close();
        }
      }
    }

    emojiToggle(){
    	//this.keyboard.hide();

      (<HTMLElement>document.querySelector("ion-footer")).setAttribute('style', 'bottom: 0');

    	this.emojiToggled = !this.emojiToggled;

    	setTimeout(function(){
        (<HTMLElement>document.querySelector("emoji-content")).setAttribute('style', 'margin-left: -20px; display:flex !important');
        (<HTMLElement>document.querySelector("ion-footer")).setAttribute('style', 'bottom: 314px');
      }, 1000);

    }

    doInfinite(infiniteScroll) {
      console.log('Begin async operation');
      this.pageNumber++;
      console.log('PAGE NUMBER', this.pageNumber);
      setTimeout(() => {

        this.globalProvider.getMessages({userId: this.userId, pagination: {pageNumber: this.pageNumber, pageSize: 20} })
        .subscribe(data => {
          this.valid = data.valid;
          if (typeof data.chatMessages !== 'undefined' && data.chatMessages.length > 0) {
            //this.chatMessages = this.chatMessages.concat(data.chatMessages);
            /*Array.prototype.push.apply(this.chatMessages, data.chatMessages);
            this.chatMessages = this.chatMessages.reverse();*/
            this.chatMessages = this.chatMessages.reverse().concat(data.chatMessages).reverse();
            this.infiniteEnabled = true;

            setTimeout(() => {
              console.log('scrolling in infinite...');
              this.content.scrollTo(0, 100);
            }, 100);

          } else {
            this.infiniteEnabled = false;
          }
        });

        console.log('Async operation has ended');
        infiniteScroll.complete();
      }, 500);
    }

    goToGoogleMap(mapData){
    	window.open('geo:0,0?q='+mapData.lat+','+mapData.lng, '_system');
    }

    getImageURL(imagePath){
    	return this.baseURL + imagePath + "_150";
    }

    public  showAlert(id,message){
      console.log(id);
      console.log(message);
          let alert = this.alertCtrl.create({
              title: 'Report User',
              inputs: [
                  {
                      name: 'reason',
                      placeholder: 'Report Here'
                  }
              ],
              buttons: [
                  {
                      text: 'Cancel',
                      role: 'cancel',
                      handler: data => {
                          console.log('Cancel clicked');
                      }
                  },
                  {
                      text: 'Report',
                      cssClass : 'ButtonCss',
                      handler: data => {
                          console.log(data.reason);
                          if(data.reason){
                              console.log("fine");
                              // this.globalProvider.reportUserById(id,data.reason)
                              //     .subscribe(data => {
                              //         console.log(data);
                              //     });
                          }else{
                              console.log("please type some Reason");
                              let toast = this.toastCtrl.create({
                                  message: 'Please type some Reason',
                                  duration: 3000,
                                  position: 'top'
                              });
                              toast.onDidDismiss(() => {
                                  console.log('Dismissed toast');
                              });
                              toast.present();
                              return false;
                          }

                      }
                  }
              ]
          });
          alert.present();
    }

}
