import { Component } from '@angular/core';
import {Events, IonicPage, NavController, NavParams, PopoverController, ToastController} from 'ionic-angular';
import {GlobalProvider} from "../../providers/global/global";
import {LoadingProvider} from "../../providers/loading/loading";

@IonicPage()
@Component({
  selector: 'page-show-picture',
  templateUrl: 'show-picture.html',
})
export class ShowPicturePage {
	pictureSrc: string;
    pictureFrom:string='';
  constructor(public events: Events,public loadingService:LoadingProvider,public popoverCtrl: PopoverController,private toastCtrl: ToastController,public navParams: NavParams,public navCtrl: NavController,public globalProvider: GlobalProvider) {
  	this.pictureSrc = this.navParams.get('pictureSrc');
      this.pictureFrom = this.navParams.get('from');
  }
ionViewWillEnter(){
  this.events.subscribe('action1', (data) => {
      console.log("first hit");
          this.makeProfile();
      });
      this.events.subscribe('action2', (data) => {
          this.deleteImage();
      });
}
ionViewWillLeave(){
    this.events.unsubscribe('action1');
    this.events.unsubscribe('action2');
}
  ionViewDidLoad() {
    console.log('ionViewDidLoad ShowPicturePage');
  }
    public makeProfile(){
        this.loadingService.showLoading("Updating Profile pic");
        var res = this.pictureSrc.substring(this.pictureSrc.lastIndexOf('/')+1);
        this.globalProvider.makeprofilepicture(res)
            .subscribe((data) => {
                this.loadingService.hideLoading();
                console.log(data);
                if(data.successful){
                      let toast = this.toastCtrl.create({
                                    message: "Profile Photo Marked Successfully",
                                    duration: 2000,
                                    position: 'middle'
                                });
                                toast.onDidDismiss(() => {
                                    this.navCtrl.pop();
                                });
                                toast.present();
                }else{
                            let toast = this.toastCtrl.create({
                                                    message: data.errors[0].description,
                                                    duration: 2000,
                                                    position: 'middle'
                                                });
                                                toast.present();
                }
            });
    }
    public deleteImage(){
        this.loadingService.showLoading("Deleting pic");
        var res = this.pictureSrc.substring(this.pictureSrc.lastIndexOf('/')+1);
        this.globalProvider.removePicture(res)
            .subscribe(data => {
                this.loadingService.hideLoading();
                if(data.successful){
                    let toast = this.toastCtrl.create({
                        message: data.message,
                        duration: 2000,
                        position: 'middle'
                    });
                    toast.onDidDismiss(() => {
                        this.navCtrl.pop();
                    });
                    toast.present();
                }else {
                    let toast = this.toastCtrl.create({
                        message: data.message,
                        duration: 2000,
                        position: 'middle'
                    });
                    toast.present();
                }

            });
    }
    presentProfileModal() {
        let profileModal = this.popoverCtrl.create('PictureActionPage');
        profileModal.present();
    }

}
