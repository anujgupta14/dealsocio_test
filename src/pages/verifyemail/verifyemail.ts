import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController} from 'ionic-angular';
import {LoadingProvider} from "../../providers/loading/loading";
import {GlobalProvider} from "../../providers/global/global";

/**
 * Generated class for the VerifyemailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-verifyemail',
  templateUrl: 'verifyemail.html',
})
export class VerifyemailPage {
    EmailType:boolean=true;
    login :any={
        'username':this.navParams.get('myEmail')
    };
  constructor(public navCtrl: NavController,public loadingService:LoadingProvider,private toastCtrl: ToastController, public navParams: NavParams, public globalProvider: GlobalProvider) {

  }

    verify() {
      console.log(this.login.username);
        this.loadingService.showLoading("Sending Email");
        this.globalProvider.sendemailotp(this.login.username).subscribe(data => {
            console.log(data);
            this.loadingService.hideLoading();
            if(data.successful){
                let toast = this.toastCtrl.create({
                    message: data.message,
                    duration: 1000,
                    position: 'top'
                });
                toast.onDidDismiss(() => {
                    this.navCtrl.pop();
                });
                toast.present();
            }
            else{
                if(data.errors[0]){
                    let toast = this.toastCtrl.create({
                        message: data.errors[0].description,
                        duration: 2000,
                        position: 'middle'
                    });
                    toast.present();
                }
            }
            });
    }
    onLogin(user) {

    }
}
