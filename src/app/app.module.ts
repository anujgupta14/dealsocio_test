import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { HttpModule} from '@angular/http';
import { TabsPage } from '../pages/tabs/tabs';
import { LoginPage } from '../pages/login/login';
import { IonicStorageModule } from '@ionic/storage';
//import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { GlobalProvider } from '../providers/global/global';
import { Geolocation } from '@ionic-native/geolocation';
import { GoogleMaps } from '../providers/google-maps/google-maps';
import { Connectivity } from '../providers/connectivity-serv/connectivity-serv';
import { Network } from '@ionic-native/network';
import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
//import { GooglePlus } from '@ionic-native/google-plus';
import {StompConfig, StompRService} from '@stomp/ng2-stompjs';
import { AutoresizeDirective } from '../providers/global/autoresize';
//import {Firebase} from '@ionic-native/firebase';
import {Facebook} from "@ionic-native/facebook";
import {PipesModule} from "../pipes/pipes.module";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {LoadingProvider} from "../providers/loading/loading";
import {LocationAccuracy} from "@ionic-native/location-accuracy";
import {Diagnostic} from "@ionic-native/diagnostic";
import { AndroidPermissions } from '@ionic-native/android-permissions';
import {Autostart} from "@ionic-native/autostart";
import {AppVersion} from '@ionic-native/app-version';
import {FCM} from "@ionic-native/fcm";

@NgModule({
  declarations: [
    MyApp,
    TabsPage,
    LoginPage,
    AutoresizeDirective,
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp,{
        menuType: 'push',
        tabsHideOnSubPages: true,
    }),
    IonicStorageModule.forRoot(),
      BrowserAnimationsModule,
      PipesModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    TabsPage,
    LoginPage,
  ],
  providers: [
   // StatusBar,
     Diagnostic,
      LocationAccuracy,
      AndroidPermissions,
    SplashScreen,
      Autostart,
      AppVersion,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    Facebook,
      LoadingProvider,
    GlobalProvider,
    Connectivity,
    GoogleMaps,
    Network,
    Geolocation,
 //   GooglePlus,
    FileTransfer,
    FileTransferObject,
    File,
    StompConfig,
    StompRService,
      FCM
   // Firebase
  ]
})
export class AppModule {}
