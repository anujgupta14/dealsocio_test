import {Platform, AlertController, App, IonicApp, NavController} from 'ionic-angular';
import {Component, ViewChild} from '@angular/core';
import {ToastController} from 'ionic-angular';
//import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { LoginPage } from '../pages/login/login';
import { GlobalProvider } from '../providers/global/global';
//import { GooglePlus } from '@ionic-native/google-plus';
//import {Firebase} from '@ionic-native/firebase';
import {MyhomePage} from "../pages/myhome/myhome";
import {Storage} from "@ionic/storage";
import {TabsPage} from "../pages/tabs/tabs";
import {Geolocation} from "@ionic-native/geolocation";
import {LoadingProvider} from "../providers/loading/loading";
import {LocationAccuracy} from "@ionic-native/location-accuracy";
import { AndroidPermissions } from '@ionic-native/android-permissions';
import {Facebook} from "@ionic-native/facebook";
import {Network} from "@ionic-native/network";
import {Autostart} from "@ionic-native/autostart";

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
    @ViewChild('myNav') nav: NavController;
    rootPage:any = '';
  public counter=0;
    public previousStatus:any;
    firebaseToken: any="";
  constructor(public fb: Facebook,private autostart: Autostart,private network: Network,private androidPermissions: AndroidPermissions,private ionicApp: IonicApp,private locationAccuracy: LocationAccuracy,public platform: Platform,public loadingService:LoadingProvider,private geolocation: Geolocation,public storage: Storage, splashScreen: SplashScreen,public globalProvider: GlobalProvider,
      private alertCtrl: AlertController, public toastCtrl: ToastController, private app: App) {
      this.platform.ready().then(() => {
          // let status bar overlay webview
         // statusBar.overlaysWebView(true);
        // set status bar to white
         // statusBar.styleBlackTranslucent();
         // statusBar.backgroundColorByHexString('#ffffff');
          this.autostart.enable();
          if(this.network.type =="none"){
              let toast = this.toastCtrl.create({
                  message: 'APP Need Internet,Please ON your Interent',
                  cssClass:"networkIssue",
                  duration: 2000,
                  position: 'bottom'
              });
              toast.onDidDismiss(() => {
                  this.rootPage = LoginPage;
              });
              toast.present();
          }
          this.network.onDisconnect().subscribe(() => {
              if (this.previousStatus === 'Online') {
                  let toast = this.toastCtrl.create({
                      message: 'Check your internet Connection!',
                      cssClass:"networkIssue",
                      duration: 3000,
                      position: 'bottom'
                  });
                  toast.present();
              }
              this.previousStatus = 'Offline';
          });
          this.network.onConnect().subscribe(() => {
              this.previousStatus = 'Online';
          });
         // this.requestGPSPermission();
          this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION).then(
              result => {
                  console.log("inside check permission");
                  console.log(result);
                  if (result.hasPermission) {

                      //If having permission show 'Turn On GPS' dialogue
                      this.askToTurnOnGPS();
                  } else {

                      //If not having permission ask for permission
                      this.requestGPSPermission();
                  }
              },
              err => {
                  alert(err);
              }
          );

          splashScreen.hide();

          // this.locationAccuracy.canRequest().then((canRequest: boolean) => {
          //
          //         // the accuracy option will be ignored by iOS
          //         this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
          //             () => console.log('Request successful'),
          //             error => this.platform.exitApp();
          //         );
          // });
          // this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.CAMERA).then(
          //     result => console.log('Has permission?',result.hasPermission),
          //     err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.CAMERA)
          // );
          //
          // this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.CAMERA, this.androidPermissions.PERMISSION.GET_ACCOUNTS]);
          // this.diagnostic.isLocationEnabled().then(
          //     (isAvailable) => {
          //           console.log(this.diagnostic.isLocationAuthorized().then(
          //               (value) => {
          //                   console.log(value);
          //               }));
          //         if(isAvailable) {
          //             // the accuracy option will be ignored by iOS
          //             this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
          //                 () => console.log('Request successful'),
          //                 error => console.log('Error requesting location permissions', error)
          //             );
          //         }else {
          //               console.log("not available");
          //         }
          //
          //     }).catch( (e) => {
          //     console.log(e);
          //     alert(JSON.stringify(e));
          // });

          // this.diagnostic.isLocationEnabled().then((isEnabled) => {
          //     console.log(isEnabled);
          //         console.log("not granted");
          //       //  this.diagnostic.switchToLocationSettings();
          //     this.locationAccuracy.canRequest().then((canRequest: boolean) => {
          //         console.log("inside location Accuracy");
          //         console.log(canRequest);
          //     this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
          //         () => console.log('Request successful'),
          //         error => this.platform.exitApp()
          //     );
          //     if(canRequest) {
          //         // the accuracy option will be ignored by iOS
          //
          //     }
          //
          // });
          //         //handle confirmation window code here and then call switchToLocationSettings
          //        // this.platform.exitApp();
          // });



          // this.globalProvider.fcmRegister(this.firebaseToken)
          //     .subscribe(fcmdata => {
          //         console.log('Token Register: ',fcmdata);
          //     });
          this.storage.get('userDetails').then((data) => {
              if(data){
                  this.globalProvider.loadConfig().subscribe(
                      (response)=>{
                          console.log(response);
                      });
                  console.log("inside constructor");
                  console.log(data);
                  console.log(data.status);
                  this.globalProvider.getexper()
                      .subscribe(data => {
                          console.log(data);
                          if(data.successful){
                              this.storage.set('exp',data.elements[0]);
                              this.rootPage = TabsPage;
                          }else if(data.status==401){
                                  let toast = this.toastCtrl.create({
                                      message: data.message,
                                      duration: 3000,
                                      cssClass: "wrong",
                                      position: 'bottom'
                                  });
                                  toast.onDidDismiss(() => {
                                      this.storage.clear();
                                      this.fb.logout();
                                      this.rootPage = LoginPage;
                                  });
                                  toast.present();
                              }

                      });


                 // console.log(this.firebase.onNotificationOpen());
                 //  this.geolocation.getCurrentPosition().then((resp) => {
                 //      console.log(resp);
                 //      console.log(resp.coords.latitude);
                 //      this.globalProvider.location = resp;
                 //      localStorage.setItem('latitude',JSON.stringify(resp.coords.latitude));
                 //      localStorage.setItem('longitude',JSON.stringify(resp.coords.longitude));
                 //      //localStorage.setItem('lat',)
                 //  }).catch((error) => {
                 //      console.log('Error getting location', error);
                 //  })
              }else{
                  this.globalProvider.loadConfig().subscribe(
                      (response)=>{
                          console.log(response);
                      });
                 this.rootPage = LoginPage;
              }
          });

      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.

      // this.firebase.onNotificationOpen().subscribe(notification => {
      //     console.log('notification info: ', notification);
      //
      //     if(notification.tap){
      //       if(notification.action == 'profile'){
      //         //this.app.getRootNav().setRoot(HomePage);
      //         console.log(this.app.getRootNav());
      //         //this.rootPage = "UserDetailPage";
      //         /*this.app.getRootNav().setRoot(UserDetailPage, {
      //           userid : notification.userId
      //         });*/
      //         this.globalProvider.notificationRedirect = {
      //           page : TabsPage,     //   "UserDetailPage"
      //           data : {
      //             userid : notification.userId
      //           }
      //         }
      //       } else if(notification.action == 'blocked'){
      //         this.rootPage = MyhomePage;
      //       } else if(notification.action == 'appopen'){
      //           this.rootPage = TabsPage;
      //       }
      //       else if(notification.action == 'flyer_request_list'){
      //       //	this.rootPage = MyhomePage;
      //       	this.globalProvider.notificationRedirect = {
	   //              page : TabsPage,    //"FlyerreqPage"
	   //              data : {
	   //                flyerId : notification.flyerId
	   //              }
	   //          }
      //       } else if(notification.action == 'message'){
      //         //window.location.reload();
      //         this.rootPage = TabsPage;
      //         this.globalProvider.notificationRedirect = {
      //           page : TabsPage,     //"ChatWindowPage"
      //           data : {
      //             userId : notification.userId,
      //             userName : "",
      //             userProfilePic : notification.image
      //           }
      //         }
      //       } else {
      //        // this.rootPage = MyhomePage;
      //       }
      //     } else {
      //       console.log('----------notification info: ', notification);
      //         this.rootPage = TabsPage;
      //       if(notification.action == 'blocked'){
      //         this.rootPage = MyhomePage;
      //       }
      //       // if(notification.action != 'message'){
      //       //   this.globalProvider.unreadNotifications++;
      //       // }
      //     }
      //
      //     /*!notification.tap
      //         ? console.log('The user was using the app when the notification arrived...')
      //         : console.log('The app was closed when the notification arrived...');
      //
      //     let notificationAlert = this.alertCtrl.create({
      //         title: notification.title,
      //         message: notification.body,
      //         buttons: ['Ok']
      //     });
      //     notificationAlert.present();*/
      // },
      // error => {
      //     console.error('Error getting the notification', error);
      // });
       splashScreen.hide();

       this.platform.registerBackButtonAction((event) => {
           console.log(this.ionicApp._overlayPortal._views[0]);
           if(this.ionicApp._overlayPortal._views[0]==undefined){
               let nav = app.getActiveNavs()[0];
               console.log(nav);
               if(this.loadingService.isLoading){
                   this.loadingService.hideLoading();
               }else {
                   if (nav._componentName === "tabs") {
                       if (this.counter == 0) {
                           this.counter++;
                           this.presentToast();
                           setTimeout(() => {
                               this.counter = 0
                           }, 3000)
                       } else {
                           // console.log("exitapp");
                           this.platform.exitApp();
                       }
                   } else {
                       let activeView = nav.getActive();
                       console.log(activeView.name);
                       console.log(nav.canGoBack());
                       console.log( this.globalProvider.makebackActive);
                       if (nav.canGoBack() || this.globalProvider.makebackActive || activeView.name === 'UserProfilePage' || activeView.name === 'UserEventPage' || activeView.name === 'SignupPage') {
                           app.navPop();
                           //nav.pop();
                           // nav.popTo( nav.getByIndex(1));
                       }else if (activeView.name === 'MyeventsPage' || activeView.name === 'AcceptedeventsPage' || activeView.name === 'MyhomePage' || activeView.name === 'SearchUserPage'  || activeView.name === 'SearchPlacesPage' || activeView.name === 'ProfilePage' || activeView.name === 'EventsPage' || activeView.name === 'ExperiancePage' || activeView.name === 'SearchPage' || activeView.name === 'LoginPage' ) {
                           //this.showAlert();
                           if (this.counter == 0) {
                               this.counter++;
                               this.presentToast();
                               setTimeout(() => {
                                   this.counter = 0
                               }, 3000)
                           } else {
                               // console.log("exitapp");
                               this.platform.exitApp();
                           }
                       }
                       else {
                           nav.pop();
                       }
                   }
               }
           }else {
               console.log(this.ionicApp._overlayPortal._views[0].isOverlay);
               this.ionicApp._overlayPortal._views[0].dismiss();// it will close the modals, alerts
           }
      }, 200);
      // this.locationAccuracy.canRequest().then((canRequest: boolean) => {
      //
      //   if(canRequest) {
      //     // the accuracy option will be ignored by iOS
      //     this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
      //       () => console.log('Request successful'),
      //       error => console.log('Error requesting location permissions', error)
      //     );
      //   }
      //
      // });

    });
  }
//   public enableLocation()
//       {
//           console.log("Inside location");
//           this.locationAccuracy.canRequest().then((canRequest: boolean) => {
// console.log(canRequest);
//               if(canRequest) {
// // the accuracy option will be ignored by iOS
//                   this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
//                       () => alert('Request successful'),
//                       error =>this.platform.exitApp()
//                   );
//               }else{
//                   console.log("inside else of enable Location");
//                   this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
//                       () => alert('Request successful'),
//                       error =>console.log("followinf Error"+JSON.stringify(error))
//                   );
//               }
//
//           });
//       }
    public showAlert() {
        let alert = this.alertCtrl.create({
            title: 'Exit?',
            message: 'Do you want to exit the app?',
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: () => {
                    }
                },
                {
                    text: 'Exit',
                    handler: () => {
                        this.platform.exitApp();
                    }
                }
            ]
        });
        alert.present();
    }

  presentToast() {
    let toast = this.toastCtrl.create({
      message: "Press again to exit",
      duration: 3000,
      position: "top"
    });
    toast.present();
  }


  async checkLogin(){
    //TODO: remove
    /*this.globalProvider.userDetails = {"session":{"token":"2915cd58-cb71-4a4f-8aab-f1329340617a", "userId": "5a3e69af5fd6bf0c51e15dcf"}};
    this.globalProvider.headers.delete("token");
    this.globalProvider.headers.delete("userid");
    this.globalProvider.headers.append("token","2915cd58-cb71-4a4f-8aab-f1329340617a");
    this.globalProvider.headers.append("userid","5a3e69af5fd6bf0c51e15dcf");
    this.rootPage = HomePage;*/
    // ./ TODO: remove

    // TODO uncomment
    try {
      //let status = await this.googlePlus.trySilentLogin({});
      console.log("User Found");
      this.rootPage = MyhomePage;
    } catch (error) {
      console.log("User Not Found.");
      this.rootPage = LoginPage;
    }
  }

    //Check if application having GPS access permission
    //     public checkGPSPermission() {
    //   console.log("inside check permission")
    //
    //
    //     }
    // public Test(){
    //     this.diagnostic.isLocationEnabled().then(function (available) {
    //         alert("Location is " + (available ? "enabled" : "not enabled"));
    //         if(available==false){
    //             this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
    //                 () => console.log("done"),
    //                 error => console.log('Error requesting location permissions', JSON.stringify(error))
    //             );
    //         }
    //     }).catch(function (error) {
    //         alert("The following error occurred: " + error);
    //     });
    //     let successCallback = (isAvailable) => { console.log("permission granted"); };
    //     let errorCallback = (e) => this.platform.exitApp();
    //
    //     this.diagnostic.isGpsLocationAvailable().then(successCallback).catch(errorCallback);
    // }
    public requestGPSPermission() {
        this.locationAccuracy.canRequest().then((canRequest: boolean) => {
            if (canRequest) {
                console.log("4");
            } else {
                //Show 'GPS Permission Request' dialogue
                //this.askToTurnOnGPS();
                this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION)
                    .then(
                        () => {
                            // call method to turn on GPS
                            this.askToTurnOnGPS();
                        },
                        error => {
                            //Show alert if user click on 'No Thanks'
                            this.platform.exitApp();
                        }
                    );
            }
        });
    }
    askToTurnOnGPS() {
      this.getLocationCoordinates();
        // let toast = this.toastCtrl.create({
        //     message: "Enable App Location ",
        //     duration: 3000,
        //     position: 'top'
        // });
        // toast.onDidDismiss(() => {
        //     this.platform.exitApp();
        // });
      //  toast.present();
        // this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
        //     () => {
        //         // When GPS Turned ON call method to get Accurate location coordinates
        //         this.getLocationCoordinates()
        //     },
        //     error => alert('Error requesting location permissions ' + JSON.stringify(error))
        // );
    }
    getLocationCoordinates() {
        this.geolocation.getCurrentPosition().then((resp) => {
           console.log(resp);
        }).catch((error) => {
            this.platform.exitApp();
        });
    }
}
